package student.service.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.servlet.PathRequest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import student.service.service.UserDetailsServiceImpl;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
	@Autowired
	private UserDetailsServiceImpl userDetailsService;

	@Autowired
	public void configureAuthentication(
			AuthenticationManagerBuilder authenticationManagerBuilder)
			throws Exception {

		authenticationManagerBuilder
				.userDetailsService(this.userDetailsService).passwordEncoder(
						passwordEncoder());
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	@Bean
	public AuthenticationTokenFilter authenticationTokenFilterBean() throws Exception {
		AuthenticationTokenFilter authenticationTokenFilter = new AuthenticationTokenFilter();
		authenticationTokenFilter.setAuthenticationManager(authenticationManagerBean());
		return authenticationTokenFilter;
	}




	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
		httpSecurity
			.csrf().disable()
			.sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
				.and()

			.authorizeRequests()
				.requestMatchers(PathRequest.toStaticResources().atCommonLocations()).permitAll()
				.antMatchers("/images/**").permitAll()
				.antMatchers("/index.html", "/api/users/login", "/api/register", "/api/documents/download/**", "/api/documents/**").permitAll()
				//.antMatchers(HttpMethod.GET, "/api/**").permitAll()
				.antMatchers(HttpMethod.GET, "/api/**").hasAnyAuthority("ADMINISTRATOR", "TEACHER", "STUDENT")
				.antMatchers(HttpMethod.POST, "/api/exam-grades/register-exam").hasAuthority("STUDENT")
				.antMatchers(HttpMethod.POST, "/api/exam-grades").hasAuthority("TEACHER")
				.antMatchers(HttpMethod.POST, "/api/**").hasAuthority("ADMINISTRATOR")
				//.antMatchers(HttpMethod.PUT, "/api/**").hasAuthority("TEACHER")
				.antMatchers(HttpMethod.PUT, "/api/**").hasAnyAuthority("ADMINISTRATOR", "TEACHER", "STUDENT")
				.antMatchers(HttpMethod.DELETE, "/api/**").hasAuthority("ADMINISTRATOR")
				.antMatchers(HttpMethod.DELETE, "/api/documents/*").hasAnyAuthority("ADMINISTRATOR", "STUDENT")
				.antMatchers("/api/documents/download/**", "/api/documents/download/*").permitAll()
				.anyRequest().authenticated();


		// Custom JWT based authentication
		httpSecurity.addFilterBefore(authenticationTokenFilterBean(),
				UsernamePasswordAuthenticationFilter.class);
	}


}
