package student.service.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import student.service.dto.*;
import student.service.dto.hybrids.TeacherDetailsDTO;
import student.service.mappers.SubjectMapper;
import student.service.mappers.TeacherMapper;
import student.service.mappers.TeacherTypeMapper;
import student.service.model.*;
import student.service.service.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value="/api/teachers")
public class TeacherController {

    @Autowired
    private TeacherService _teacherService;
    @Autowired
    private TeacherTypeService _teacherTypeService;
    @Autowired
    private UserService _userService;
    @Autowired
    private UserAuthorityService _userAuthorityService;
    @Autowired
    private AuthorityService _authorityService;
    @Autowired
    private SubjectService _subjectService;

    @GetMapping
    public ResponseEntity<Page<TeacherDetailsDTO>> findAll(@RequestParam(value = "page", defaultValue = "1") int page,
                                                    @RequestParam(value = "size", defaultValue = "5") int size)
    {
        Pageable pageableRequest = PageRequest.of(page - 1 , size);
        Page<User> teachers = _userAuthorityService.findTeachers(pageableRequest).map(x -> x.getUser());
        Page<TeacherDetailsDTO> teachersDTO = TeacherDetailsDTO.toDTOS(teachers);
        return new ResponseEntity<>(teachersDTO, HttpStatus.OK);
    }

    @GetMapping(value="/{id}")
    public ResponseEntity<TeacherDetailsDTO> findById(@PathVariable("id") Long id)
    {
        User user = _userService.findById(id);
        if(user != null){
            if(user.getTeacher() != null){
                List<Subject> subjects = _subjectService.findByTeacher(user.getTeacher().getTeacherCode());
                List<SubjectDTO> subjectDTOS = SubjectMapper.toDtos(subjects);
                TeacherDetailsDTO dto = new TeacherDetailsDTO(user, subjectDTOS);

                return new ResponseEntity<>(dto, HttpStatus.OK);
            }
            return new ResponseEntity<>(new TeacherDetailsDTO(), HttpStatus.OK);
        }
        return new ResponseEntity<>(new TeacherDetailsDTO(), HttpStatus.OK);
    }

    @PostMapping(consumes = "application/json")
    public ResponseEntity<TeacherDTO> create(@RequestBody CreateTeacherDTO createTeacherDTO)
    {
        TeacherType type = _teacherTypeService.findById(createTeacherDTO.getTeacherTypeId());
        Teacher teacher = new Teacher(null, false, type, null);
        teacher.setTeacherType(type);
        teacher = _teacherService.save(teacher);
        User user = _userService.findById(createTeacherDTO.getUserId());
        user.setTeacher(teacher);
        _userService.save(user);
        Authority authority = _authorityService.findByName("TEACHER");
        UserAuthority userAuthority = new UserAuthority(user, authority);
        _userAuthorityService.save(userAuthority);
        TeacherDTO teacherDTO = TeacherMapper.toDto(teacher);
        return new ResponseEntity<>(teacherDTO, HttpStatus.OK);
    }

    @PutMapping(value = "/{id}", consumes = "application/json")
    public ResponseEntity<TeacherEditDTO> update(@RequestBody TeacherEditDTO teacherDTO, @PathVariable("id") Long id) {
        User teacher = _userService.findById(id);
        if(teacher == null)
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        teacher.setFirstname(teacherDTO.getFirstname());
        teacher.setLastname(teacherDTO.getLastname());
        teacher.setEmail(teacherDTO.getEmail());
        teacher.setAddress(teacherDTO.getAddress());
        _userService.save(teacher);

        TeacherType teacherType = _teacherTypeService.findByName(teacherDTO.getTeacherType());
        teacher.getTeacher().setTeacherType(teacherType);
        _teacherService.save(teacher.getTeacher());
        return new ResponseEntity<>(teacherDTO, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") Long id) {
        User user = _userService.findById(id);
        if (user != null) {
            if(user.getTeacher() != null){
                _userService.deleteTeacher(user);
                return new ResponseEntity<>(HttpStatus.OK);
            }
            else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}
