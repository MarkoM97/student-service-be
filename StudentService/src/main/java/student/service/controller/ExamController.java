package student.service.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import student.service.dto.*;
import student.service.dto.hybrids.ExamRegistrationDetailsDTO;
import student.service.dto.hybrids.SubjectRegistrationDetailsDTO;
import student.service.dto.hybrids.TeacherExamDTO;
import student.service.mappers.*;
import student.service.model.*;
import student.service.service.*;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api/exams")
public class ExamController {

    @Autowired
    private ExamService _examService;

    @Autowired
    private SubjectService _subjectService;

    @Autowired
    private ExaminationPeriodService _examinationPeriodService;

    @Autowired
    private ExamGradeService _examGradeService;

    @Autowired
    private UserService _userService;

    @Autowired
    private TeacherService _teacherService;

    @GetMapping
    public ResponseEntity<Page<ExamDTO>> findAll(@RequestParam(value = "page", defaultValue = "1") int page,
                                                 @RequestParam(value = "size", defaultValue = "2") int size)
    {
        Pageable pageableRequest = PageRequest.of(page - 1, size);
        Page<Exam> exams = _examService.findAllPageable(pageableRequest);
        Page<ExamDTO> examsTO = ExamMapper.pageableToDtos(exams);

        return new ResponseEntity<>(examsTO, HttpStatus.OK);
    }

    @GetMapping(value = "/detailed")
    public ResponseEntity<Page<ExamDetailsDTO>> findAllDetailed(@RequestParam(value = "page", defaultValue = "0") int page,
                                                 @RequestParam(value = "limit", defaultValue = "5") int limit)
    {
        Pageable pageableRequest = PageRequest.of(page, limit);
        Page<Exam> exams = _examService.findAllPageable(pageableRequest);
        Page<ExamDetailsDTO> examsDTO = ExamDetailsMapper.pageableToDtos(exams);

        return new ResponseEntity<>(examsDTO, HttpStatus.OK);
    }

    @GetMapping(value = "/all-detailed")
    public ResponseEntity<List<ExamDetailsDTO>> findAll()
    {
        List<Exam> exams = _examService.findAll();
        List<ExamDetailsDTO> examsDTO = ExamDetailsMapper.toDtos(exams);

        return new ResponseEntity<>(examsDTO, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<ExamDTO> findById(@PathVariable("id") Long id) {
        Exam exam = _examService.findById(id);
        ExamDTO examDTO = ExamMapper.toDto(exam);

        return new ResponseEntity<>(examDTO, HttpStatus.OK);
    }

    @GetMapping(value = "/teacher/{id}")
    public ResponseEntity<List<TeacherExamDTO>> findAllExamsForTeacher(@PathVariable("id") Long id) {
        User user = _userService.findById(id);
        List<TeacherExamDTO> examDTOS = new ArrayList<>();
        if(user != null) {
            if (user.getTeacher() != null) {
                List<Exam> exams = _examService.findByTeacher(user.getTeacher().getTeacherCode());
                exams.sort(Comparator.comparing(x -> x.getDate()));
                examDTOS = TeacherExamMapper.toDTOs(exams);

                return new ResponseEntity<>(examDTOS, HttpStatus.OK);
            }
            return new ResponseEntity<>(examDTOS, HttpStatus.OK);
        }
        return new ResponseEntity<>(examDTOS, HttpStatus.OK);
    }

    @GetMapping(value = "/current-exams/teacher/{id}")
    public ResponseEntity<List<TeacherExamDTO>> getCurrentExamsForTeacher(@PathVariable("id") Long id) {
        User user = _userService.findById(id);
        List<TeacherExamDTO> examDTOS = new ArrayList<>();
        if(user != null){
            if(user.getTeacher() != null){
                List<ExaminationPeriod> examinationPeriods = _examinationPeriodService.findCurrentPeriod();
                List<Exam> exams = new ArrayList<>();
                examinationPeriods.stream().forEach(ep -> exams.addAll(ep.getExams()));

                for (Iterator<Exam> iterator = exams.iterator(); iterator.hasNext();) {
                    Exam exam = iterator.next();
                    if(exam.getTeacher().getTeacherCode() != user.getTeacher().getTeacherCode()){
                        iterator.remove();
                    }
                }

                exams.sort(Comparator.comparing(x -> x.getDate()));
                examDTOS = TeacherExamMapper.toDTOs(exams);

                return new ResponseEntity<>(examDTOS, HttpStatus.OK);
            }
            return new ResponseEntity<>(examDTOS, HttpStatus.OK);
        }
        return new ResponseEntity<>(examDTOS, HttpStatus.OK);
    }

    @GetMapping(value = "/registered-exams/user/{id}")
    public ResponseEntity<ExamRegistrationDetailsDTO> getRegisteredExams(@PathVariable("id") Long id) {
        User user = _userService.findById(id);
        if(user != null){
            if(user.getStudent() != null){
                List<ExaminationPeriod> examinationPeriods = _examinationPeriodService.findCurrentPeriod();
                List<ExamGrade> registeredExams = _examGradeService.findByStudentRegistered(user.getStudent().getId());

                for (Iterator<ExamGrade> iterator = registeredExams.iterator(); iterator.hasNext();) {
                    boolean samePeriod = false;
                    ExamGrade examgrade = iterator.next();

                    for (ExaminationPeriod period: examinationPeriods) {
                        if (examgrade.getExam().getExaminationPeriod().getId() == period.getId()) {
                            samePeriod = true;
                            break;
                        }
                    }
                    if(samePeriod == false){
                        iterator.remove();
                    }
                }
                List<User> teachers = new ArrayList<>();
                registeredExams.stream().forEach( examGrade -> teachers.add(
                        _userService.findByTeacher(examGrade.getExam().getTeacher().getTeacherCode())));
                List<String> teachersNames = teachers.stream().map(teacher -> new String
                        (teacher.getFirstname() + " " + teacher.getLastname()))
                        .collect(Collectors.toList());
                List<Exam> exams = registeredExams.stream().map(examGrade -> examGrade.getExam()).collect(Collectors.toList());
                List<Long> registeredExamsId = registeredExams.stream().map(examGrade ->  examGrade.getExam().getId()).collect(Collectors.toList());
                List<SubjectRegistrationDetailsDTO> detailsDTOs =  SubjectRegistrationDetailsDTO.toDTOs(exams, teachersNames, registeredExamsId);
                ExamRegistrationDetailsDTO examDTO = new ExamRegistrationDetailsDTO("", detailsDTOs, user.getStudent().getBalance());

                return new ResponseEntity<>(examDTO, HttpStatus.OK);
            }

            return new ResponseEntity<>(new ExamRegistrationDetailsDTO(), HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(new ExamRegistrationDetailsDTO(), HttpStatus.BAD_REQUEST);

    }

    @GetMapping(value = "/user/{id}")
    public ResponseEntity<List<ExamPassedDTO>> findAllExamsForStudent(@PathVariable("id") Long id){
        List<ExamPassedDTO> examsDTO = new ArrayList<>();
        List<User> teachers = new ArrayList<>();

        User student = _userService.findById(id);
        if(student != null){
            if(student.getStudent() != null){
                List<ExamGrade> examGrades = _examGradeService.findByStudent(student.getStudent().getId());

                examGrades.stream().forEach( examGrade -> teachers.add(
                        _userService.findByTeacher(examGrade.getExam().getTeacher().getTeacherCode())));

                List<String> teachersNames = teachers.stream().map(teacher -> new String
                        (teacher.getFirstname() + " " + teacher.getLastname()))
                        .collect(Collectors.toList());
                examsDTO = ExamPassedMapper.toDTOs(examGrades, teachersNames);
                return new ResponseEntity<>(examsDTO, HttpStatus.OK);
            }
            return new ResponseEntity<>(examsDTO, HttpStatus.OK);
        }
        return new ResponseEntity<>(examsDTO, HttpStatus.OK);
    }

    @PostMapping(consumes = "application/json")
    public ResponseEntity<ExamDetailsDTO> create(@RequestBody ExamDetailsDTO examDTO) {
        Subject subject = _subjectService.findById(examDTO.getSubject().getId());
        Exam exam = new Exam(null,examDTO.getDate(), examDTO.getTime(), examDTO.getClassRoom(), false, subject.getTeacher(),null, subject,null);
        exam = _examService.save(exam);
        examDTO.setId(exam.getId());

        return new ResponseEntity<>(examDTO, HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}", consumes = "application/json")
    public ResponseEntity<ExamDTO> update(@RequestBody ExamDetailsDTO examDTO, @PathVariable("id") Long id) {
        Subject subject = _subjectService.findById(examDTO.getSubject().getId());
        Teacher teacher = _teacherService.findByTeacherCode(subject.getTeacher().getTeacherCode());
        Exam exam = _examService.findById(id);
        exam.setDate(examDTO.getDate());
        exam.setTime(examDTO.getTime());
        exam.setClassRoom(examDTO.getClassRoom());
        exam.setSubject(subject);
        exam.setTeacher(teacher);
        _examService.save(exam);

        return new ResponseEntity<>(examDTO, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    //@PreAuthorize("hasRole('ROLE_ADMINISTRATOR')")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        Exam exam = _examService.findById(id);
        if (exam != null) {
            _examService.remove(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
