package student.service.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import student.service.dto.*;
import student.service.dto.hybrids.StudentDetailsDTO;
import student.service.mappers.CourseAttendingMapper;
import student.service.mappers.PaymentMapper;
import student.service.model.CourseAttending;
import student.service.model.Student;
import student.service.model.StudyProgram;
import student.service.model.User;
import student.service.service.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(value="/api/students")
public class StudentController {

    @Autowired
    private StudentService _studentService;

    @Autowired
    private UserService _userService;

    @Autowired
    private CourseAttendingService _courseAttendingService;

    @Autowired
    private StudyProgramService studyProgramService;

    @Autowired
    private UserAuthorityService _userAuthorityService;

    @Autowired
    private StudyProgramService _studyProgramService;

    @Autowired
    private PasswordEncoder passwordEncoder;


    @GetMapping
    public ResponseEntity<Page<StudentDetailsDTO>> findAll(@RequestParam(value = "page", defaultValue = "1") int page,
                                                    @RequestParam(value = "size", defaultValue = "5") int size)
    {
        Pageable pageableRequest = PageRequest.of(page - 1, size);
        Page<User> students = _userAuthorityService.findStudents(pageableRequest).map(x -> x.getUser());
        List<CourseAttending> courseAttending = new ArrayList<>();
        students.forEach(x -> courseAttending.add(_courseAttendingService.findByStudentId(x.getStudent().getId())));
        List<CourseAttendingDTO> courseAttendingDTOS = CourseAttendingMapper.toDtos(courseAttending);
        Page<StudentDetailsDTO> studentsDTO = StudentDetailsDTO.toDTOS(students, courseAttendingDTOS);

        return new ResponseEntity<>(studentsDTO, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<StudentDetailsDTO> findById(@PathVariable("id") long id) {
        User user = _userService.findByStudent(id);
        if(user == null)
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        CourseAttending courseAttending = _courseAttendingService.findByStudentId(user.getStudent().getId());
        CourseAttendingDTO courseAttendingDTO = CourseAttendingMapper.toDto(courseAttending);
        List<PaymentDTO> payments = PaymentMapper.toDtos(user.getStudent().getPayments());
        StudentDetailsDTO studentDetailsDTO = new StudentDetailsDTO(user, courseAttendingDTO, payments);
        return new ResponseEntity<>(studentDetailsDTO, HttpStatus.OK);

    }

    @GetMapping(value = "/user/{id}")
    public ResponseEntity<StudentDetailsDTO> findByUserId(@PathVariable("id") long id) {
        User user = _userService.findById(id);
        if(user == null)
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        CourseAttending courseAttending = _courseAttendingService.findByStudentId(user.getStudent().getId());
        CourseAttendingDTO courseAttendingDTO = CourseAttendingMapper.toDto(courseAttending);
        List<PaymentDTO> payments = PaymentMapper.toDtos(user.getStudent().getPayments());
        StudentDetailsDTO studentDetailsDTO = new StudentDetailsDTO(user, courseAttendingDTO, payments);
        return new ResponseEntity<>(studentDetailsDTO, HttpStatus.OK);

    }

    @PostMapping(consumes = "application/json")
    public ResponseEntity<CreateStudenrtDTO> create(@RequestBody CreateStudenrtDTO studentDTO) {
        User user = new User();
        Student student = new Student();
        user.setUsername(studentDTO.getUsername());
        if(studentDTO.getBirthday() == null){
            user.setBirthday(new Date());
        }else{
            user.setBirthday(studentDTO.getBirthday());
        }
        user.setFirstname(studentDTO.getFirstname());
        user.setLastname(studentDTO.getLastname());
        user.setEmail(studentDTO.getEmail());
        user.setAddress(studentDTO.getAddress());
        user.setDeleted(false);
        user.setPassword(studentDTO.getPassword());
        user.setPassword(passwordEncoder.encode(studentDTO.getPassword()));

        student.setBalance(studentDTO.getBalance());
        student.setModelNumber(studentDTO.getModelNumber());
        student.setCardNumber(studentDTO.getCardNumber());
        student.setAccountNumber(studentDTO.getAccountNumber());
        student.setDeleted(false);

        student = _studentService.save(student);
        user.setStudent(student);
        user = _userService.save(user);

        CourseAttending courseAttending = new CourseAttending();
        courseAttending.setDeleted(false);
        courseAttending.setStudent(student);
        courseAttending.setDateOfEnrollment(new Date());
        StudyProgram studyProgram = _studyProgramService.findByName(studentDTO.getStudyProgram());
        courseAttending.setStudyProgram(studyProgram);

        _courseAttendingService.save(courseAttending);

        return new ResponseEntity<>(studentDTO, HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}", consumes = "application/json")
    public ResponseEntity<EditStudentDTO> update(@RequestBody EditStudentDTO studentDTO, @PathVariable("id") long id) {
        User student = _userService.findById(id);
        if(student == null)
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        student.setFirstname(studentDTO.getFirstname());
        student.setLastname(studentDTO.getLastname());
        student.setEmail(studentDTO.getEmail());
        student.setAddress(studentDTO.getAddress());
        student.getStudent().setBalance(studentDTO.getBalance());
        student.getStudent().setModelNumber(studentDTO.getModelNumber());
        student.getStudent().setCardNumber(studentDTO.getCardNumber());
        student.getStudent().setAccountNumber(studentDTO.getAccountNumber());
        CourseAttending courseAttending = _courseAttendingService.findByStudentId(student.getStudent().getId());
        StudyProgram studyProgram = _studyProgramService.findByName(studentDTO.getStudyProgram());
        courseAttending.setStudyProgram(studyProgram);

        _userService.save(student);
        _courseAttendingService.save(courseAttending);
        _studentService.save(student.getStudent());

        return new ResponseEntity<>(studentDTO, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") Long id) {
        User user = _userService.findById(id);
        if (user != null) {
            if(user.getStudent() != null){
                _userService.deleteStudent(user);
                return new ResponseEntity<>(HttpStatus.OK);
            }
            else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
