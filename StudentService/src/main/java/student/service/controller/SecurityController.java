package student.service.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/security")
public class SecurityController {

	
    @RequestMapping(value = "/home")
    public String home() {
        return "Dostupno bez auth";
    }
    
    @RequestMapping(value = "/secure")
    public String secure() {
        return "Mis prdi farba";
    }
    
    @RequestMapping(value = "/admin")
    @PreAuthorize("hasRole('ROLE_ADMINISTRATOR')")
    public String admin() {
    	return "Samo za odabrane";
    }
    
    @RequestMapping(value = "/logged-in")
    public String currentlyLoggedIn() {
    	String userName = null;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            userName = ((UserDetails)principal).getUsername();
        } else {
            userName = principal.toString();
        }
        return  userName;
    }
}
