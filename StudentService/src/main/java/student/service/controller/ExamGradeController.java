package student.service.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import student.service.dto.ExamGradeDTO;
import student.service.dto.ExamPassedDTO;
import student.service.dto.ExamRegistrationDTO;
import student.service.dto.hybrids.TeacherExamGradesDTO;
import student.service.mappers.*;
import student.service.model.Exam;
import student.service.model.ExamGrade;
import student.service.model.Student;
import student.service.model.User;
import student.service.service.ExamGradeService;
import student.service.service.ExamService;
import student.service.service.StudentService;
import student.service.service.UserService;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api/exam-grades")
public class ExamGradeController {

    @Autowired
    private ExamGradeService _examGradeService;

    @Autowired
    private UserService _userService;

    @Autowired
    private StudentService _studentService;

    @Autowired
    private ExamService _examService;

    @GetMapping
    public ResponseEntity<Page<ExamGradeDTO>> findAll(@RequestParam(value = "page", defaultValue = "1") int page,
                                                      @RequestParam(value = "size", defaultValue = "5") int size)
    {
        Pageable pageableRequest = PageRequest.of(page - 1, size);
        Page<ExamGrade> examGrades = _examGradeService.findAll(pageableRequest);
        Page<ExamGradeDTO> examGradesDTO = ExamGradeMapper.pageableToDtos(examGrades);

        return new ResponseEntity<>(examGradesDTO, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<ExamGradeDTO> findById(@PathVariable("id") Long id) {
        ExamGrade examGrade = _examGradeService.findById(id);
        ExamGradeDTO examGradeDTO = ExamGradeMapper.toDto(examGrade);

        return new ResponseEntity<>(examGradeDTO, HttpStatus.OK);
    }


    @GetMapping(value = "/user/{id}")
    public ResponseEntity<List<ExamPassedDTO>> findByUser(@PathVariable("id") Long id) {
        User user = _userService.findById(id);
        if(user != null){
            List<ExamGrade> examGrades = _examGradeService.findByStudentPassed(user.getStudent().getId());
            List<User> teachers = new ArrayList<>();
            examGrades.stream().forEach( examGrade -> teachers.add(
                    _userService.findByTeacher(examGrade.getExam().getTeacher().getTeacherCode())));
            List<String> teachersNames = teachers.stream().map(teacher -> new String
                    (teacher.getFirstname() + " " + teacher.getLastname()))
                    .collect(Collectors.toList());
            List<ExamPassedDTO> examPassedDTOS = ExamPassedMapper.toDTOs(examGrades, teachersNames);
            return new ResponseEntity<>(examPassedDTOS, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(value = "/teacher/exam/{id}")
        public ResponseEntity<List<TeacherExamGradesDTO>> findExamGradesForTeacher(@PathVariable("id") Long id) {
        List<User> users = new ArrayList<>();
            List<ExamGrade> examGrades = _examGradeService.findByExamId(id);

        examGrades.stream().forEach( examGrade -> users.add(
                _userService.findByStudent(examGrade.getStudent().getId())));
        List<String> userNames = users.stream().map(user -> new String
                (user.getFirstname() + " " + user.getLastname()))
                .collect(Collectors.toList());

        List<TeacherExamGradesDTO> dtos = TeacherExamGradesMapper.toDtos(examGrades, userNames);
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping(consumes = "application/json")
    public ResponseEntity<List<TeacherExamGradesDTO>> create(@RequestBody List<TeacherExamGradesDTO> examGradesDTOS) {
        List<Student> students = examGradesDTOS.stream().map(dto -> _studentService.getById(dto.getStudentId()))
                .collect(Collectors.toList());
        Exam exam = _examService.findById(examGradesDTOS.get(0).getExamId());

        List<ExamGrade> examGrades = TeacherExamGradesMapper.toEntities(examGradesDTOS, students, exam);
        examGrades = _examGradeService.saveAll(examGrades);
        List<TeacherExamGradesDTO> dto = TeacherExamGradesMapper.toDtos(examGrades, examGradesDTOS.stream().map(eg -> eg.getStudentName()).collect(Collectors.toList()));
        return new ResponseEntity<>(dto, HttpStatus.CREATED);
    }


    @PostMapping(value = "/register-exam", consumes = "application/json")
    @ResponseBody
    public ResponseEntity<HttpStatus> registerExam(@RequestBody ExamRegistrationDTO dto)
    {
        Exam exam = _examService.findById(dto.getExamId());
        User student = _userService.findById(dto.getStudentId());
        if(exam != null && student != null){
            ExamGrade examGrade = new ExamGrade(null, 0, new Float(0), false, student.getStudent(), exam);
            examGrade = _examGradeService.save(examGrade);
            student.getStudent().setBalance(student.getStudent().getBalance() -200);
            _studentService.save(student.getStudent());
            return new ResponseEntity<>(HttpStatus.CREATED);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @PutMapping(value = "/{id}", consumes = "application/json")
    public ResponseEntity<ExamGradeDTO> update(@RequestBody ExamGradeDTO examGradeDTO, @PathVariable("id") Long id) {
        ExamGrade examGrade = _examGradeService.findById(id);
        examGrade.setGrade(examGradeDTO.getGrade());
        examGrade.setPoints(examGradeDTO.getPoints());

        _examGradeService.save(examGrade);

        return new ResponseEntity<>(examGradeDTO, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    @PreAuthorize("hasRole('ROLE_ADMINISTRATOR')")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        ExamGrade studyProgram = _examGradeService.findById(id);
        if (studyProgram != null) {
            _examGradeService.remove(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
