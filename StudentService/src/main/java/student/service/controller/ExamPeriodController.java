package student.service.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import student.service.dto.CreateExaminationPeriodDTO;
import student.service.dto.ExaminationPeriodDTO;
import student.service.dto.hybrids.ExamRegistrationDetailsDTO;
import student.service.dto.hybrids.SubjectRegistrationDetailsDTO;
import student.service.mappers.ExaminationPeriodMapper;
import student.service.mappers.ExamMapper;
import student.service.mappers.StudentMapper;
import student.service.model.Exam;
import student.service.model.ExaminationPeriod;
import student.service.service.ExamService;
import student.service.service.ExaminationPeriodService;
import student.service.model.*;
import student.service.service.*;

@RestController
@RequestMapping(value = "/api/exam-periods")
public class ExamPeriodController {

    @Autowired
    private ExaminationPeriodService _examinationPeriodService;

    @Autowired
    private ExamService _examService;

    @Autowired
    private UserService _userService;

    @Autowired
    private ExamGradeService _examGradeService;

    @Autowired
    private StudentService _studentService;

    @Autowired
    private CourseAttendingService _courseAttendingService;

    @Autowired
    private SubjectService _subjectService;

    @GetMapping
    public ResponseEntity<Page<ExaminationPeriodDTO>> findAll(@RequestParam(value = "page", defaultValue = "1") int page,
                                                              @RequestParam(value = "size", defaultValue = "5") int size)
    {
        Pageable pageableRequest = PageRequest.of(page - 1, size);
        Page<ExaminationPeriod> examinationPeriods = _examinationPeriodService.findAll(pageableRequest);
        Page<ExaminationPeriodDTO> examinationPeriodsDTO = ExaminationPeriodMapper.pageableToDtos(examinationPeriods);
        return new ResponseEntity<>(examinationPeriodsDTO, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<ExaminationPeriodDTO> findById(@PathVariable("id") Long id) {
        ExaminationPeriod examinationPeriod = _examinationPeriodService.findById(id);
        ExaminationPeriodDTO examinationPeriodDTO = ExaminationPeriodMapper.toDto(examinationPeriod);

        return new ResponseEntity<>(examinationPeriodDTO, HttpStatus.OK);
    }

    @GetMapping(value = "/current-period/student/{id}")
    public ResponseEntity<List<ExamRegistrationDetailsDTO>> currentPeriod(@PathVariable("id") Long id) {
        List<ExamRegistrationDetailsDTO> examinationPeriodDTOS = new ArrayList<>();
        List<Subject> subjectsAttenging = new ArrayList<>();

        User student = _userService.findById(id);
        if (student != null) {
            if(student.getStudent() != null){
                Float studentBalance = _studentService.findById(student.getStudent().getId()).getBalance();
                CourseAttending studentAttending = _courseAttendingService.findByStudentId(student.getStudent().getId());

                //pronalazi sve predmete koje student pohadja
                subjectsAttenging = _subjectService.findByStudyProgram(studentAttending.getStudyProgram().getId());


                //pronalazi trenutni ispitni rok
                List<ExaminationPeriod> examinationPeriods = _examinationPeriodService.findCurrentPeriod();
                for (ExaminationPeriod ep: examinationPeriods) {
                    List<User> teachers = new ArrayList<>();
                    //proalazi sve ispite u trenutnom roku
                    List<Exam> exams = _examService.findByExaminationPeriod_Id(ep.getId());

                    //pronalazi polozene ispite
                    List<ExamGrade> examGrades = _examGradeService.findByStudentPassed(student.getStudent().getId());

                    //brisanje ispita cije predmete student nema
                    for (Iterator<Exam> itExam = exams.iterator(); itExam.hasNext();) {
                        boolean sameSubject = false;
                        Exam exam = itExam.next();
                        for (Iterator<Subject> itSubject = subjectsAttenging.iterator(); itSubject.hasNext();) {
                            Subject subject = itSubject.next();
                            if(exam.getSubject().getId() == subject.getId()){
                                sameSubject = true;
                                break;
                            }
                        }
                        if(sameSubject == false){

                            itExam.remove();
                        }
                        //brisanje ispita cije predmete je student vec polozio
                        else{
                            for (Iterator<ExamGrade> itExamGrade = examGrades.iterator(); itExamGrade.hasNext();) {
                                ExamGrade examgrade = itExamGrade.next();

                                if (examgrade.getExam().getSubject().getId() == exam.getSubject().getId()){
                                    itExam.remove();
                                    break;
                                }
                            }
                        }
                    }

                    exams.stream().forEach( exam -> teachers.add(
                            _userService.findByTeacher(exam.getTeacher().getTeacherCode())));
                    List<String> teachersNames = teachers.stream().map(teacher -> new String
                            (teacher.getFirstname() + " " + teacher.getLastname()))
                            .collect(Collectors.toList());
                    List<Long> registeredExamsId = _examGradeService.findByStudentRegistered(student.getStudent().getId()).stream().map(eg -> eg.getExam().getId())
                            .collect(Collectors.toList());
                    List<SubjectRegistrationDetailsDTO> detailsDTOs =  SubjectRegistrationDetailsDTO.toDTOs(exams, teachersNames, registeredExamsId);
                    examinationPeriodDTOS.add(new ExamRegistrationDetailsDTO(ep.getName(), detailsDTOs, studentBalance));
                }

                return new ResponseEntity<>(examinationPeriodDTOS, HttpStatus.OK);
            }
            return new ResponseEntity<>(examinationPeriodDTOS, HttpStatus.OK);
        }

        return new ResponseEntity<>(examinationPeriodDTOS, HttpStatus.OK);
    }

    @PostMapping(consumes = "application/json")
    public ResponseEntity<CreateExaminationPeriodDTO> create(@RequestBody CreateExaminationPeriodDTO examinationPeriodDTO) {
        List<Exam> exams = examinationPeriodDTO.getExams().stream().map(n -> _examService.findById(n)).collect(Collectors.toList());
        ExaminationPeriod newEP = new ExaminationPeriod(null,examinationPeriodDTO.getName(), examinationPeriodDTO.getStartDate(), examinationPeriodDTO.getEndDate(), false, new ArrayList<>());
        newEP = _examinationPeriodService.save(newEP);
        for (Long examId: examinationPeriodDTO.getExams()) {
            Exam exam = _examService.findById(examId);
            exam.setExaminationPeriod(newEP);
            exam = _examService.save(exam);
            newEP.getExams().add(exam);
        }
        examinationPeriodDTO.setId(newEP.getId());

        return new ResponseEntity<>(examinationPeriodDTO, HttpStatus.CREATED);
    }


    @PutMapping(value = "/{id}", consumes = "application/json")
    public ResponseEntity<CreateExaminationPeriodDTO> update(@RequestBody CreateExaminationPeriodDTO examinationPeriodDTO, @PathVariable("id") Long id) {
        List<Exam> exams = examinationPeriodDTO.getExams().stream().map(n -> _examService.findById(n)).collect(Collectors.toList());
        ExaminationPeriod examinationPeriod = _examinationPeriodService.findById(id);
        
        examinationPeriod.setEndDate(examinationPeriodDTO.getEndDate());
        examinationPeriod.setStartDate(examinationPeriodDTO.getStartDate());
        examinationPeriod.setName(examinationPeriodDTO.getName());
        examinationPeriod.setExams(exams);
        _examinationPeriodService.save(examinationPeriod);

        return new ResponseEntity<>(examinationPeriodDTO, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    //@PreAuthorize("hasRole('ROLE_ADMINISTRATOR')")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        ExaminationPeriod studyProgram = _examinationPeriodService.findById(id);
        if (studyProgram != null) {
            _examinationPeriodService.remove(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
