package student.service.controller;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import student.service.dto.DocumentDTO;
import student.service.mappers.DocumentMapper;
import student.service.mappers.StudentMapper;
import student.service.model.Document;
import student.service.model.Student;
import student.service.service.DocumentService;
import student.service.service.StudentService;
import student.service.service.UserService;
import sun.nio.ch.IOUtil;

import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.PersistenceUtil;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.function.Supplier;

@RestController
@RequestMapping(value = "/api/documents")
public class DocumentController {


    private static String STATIC_DIR = "src/main/resources/static/images/";


    @Autowired
    private DocumentService _documentService;


    @Autowired
    private UserService _userService;

    @Autowired
    private StudentService _studentService;

    @GetMapping
    public ResponseEntity<Page<DocumentDTO>> findAll(@RequestParam(value = "page", defaultValue = "1") int page,
                                                     @RequestParam(value = "size", defaultValue = "2") int size)
    {
        Pageable pageableRequest = PageRequest.of(page - 1, size);
        Page<Document> documents = _documentService.findAll(pageableRequest);
        Page<DocumentDTO> documentsDTO = DocumentMapper.pageableToDtos(documents);

        return new ResponseEntity<>(documentsDTO, HttpStatus.OK);
    }

    @GetMapping(value = "/by-student-id")
    public ResponseEntity<List<DocumentDTO>> findByStudent(@RequestParam(value = "studentId") String studentId)
    {
        List<Document> documents = _documentService.findByStudentId(_userService.findById(Long.valueOf(studentId).longValue()).getStudent().getId());
        List<DocumentDTO> documentsDTO = DocumentMapper.toDtos(documents);
        return new ResponseEntity<List<DocumentDTO>>(documentsDTO, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<DocumentDTO> findById(@PathVariable("id") Long id) {
        Document document = _documentService.findById(id);
        DocumentDTO documentDTO = DocumentMapper.toDto(document);

        return new ResponseEntity<>(documentDTO, HttpStatus.OK);
    }

    @PostMapping(consumes = "application/json")
    public ResponseEntity<DocumentDTO> create(@RequestBody DocumentDTO documentDTO) {
        Document document = DocumentMapper.toEntity(documentDTO);
        document = _documentService.save(document);
        documentDTO.setId(document.getId());

        return new ResponseEntity<>(documentDTO, HttpStatus.CREATED);
    }

    @PostMapping(value="/upload")
    public ResponseEntity<String> upload(@RequestParam("file") MultipartFile file, @RequestParam("studentId") String studentId) {
        try {
            String name = file.getOriginalFilename();

            Student student = _studentService.getById(_userService.findById(Long.valueOf(studentId).longValue()).getStudent().getId());
            if(student != null) {
                Document document = new Document();
                document.setName(name);
                document.setStudent(student);
                _documentService.save(document);


                String[] nameFragments = name.split("\\.");
                String fileExtension = nameFragments[nameFragments.length - 1];
                String filename = new StringBuilder().append(STATIC_DIR).append(document.getId()).append(".").append(fileExtension).toString();
                Path path = Paths.get(filename);


                byte[] bytes = file.getBytes();
                Files.write(path, bytes);
                return new ResponseEntity<String>( "successfull upload!", HttpStatus.OK);
            } {
                throw new EntityNotFoundException();
            }
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity<String>( "Upload failed", HttpStatus.BAD_REQUEST);
        } catch (EntityNotFoundException f) {
            return new ResponseEntity<String>( "Student does not exist!", HttpStatus.NOT_FOUND);
        }
    }


    @GetMapping(value="/download/{id}")
    public ResponseEntity<Object> download(@PathVariable("id") long id) throws IOException {

        Document document = _documentService.getById(id);
        if(document == null) {
            return new ResponseEntity("Document doesn't exist", HttpStatus.NOT_FOUND);
        }
        String[] nameFragments = document.getName().split("\\.");
        String filename = new StringBuilder().append(document.getId()).append(".").append(nameFragments[nameFragments.length - 1]).toString();
        File file = new File(STATIC_DIR + filename);

        HttpHeaders header = new HttpHeaders();
        header.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + document.getName());

        Path path = Paths.get(file.getAbsolutePath());
        ByteArrayResource resource = new ByteArrayResource(Files.readAllBytes(path));

        return ResponseEntity.ok()
                .headers(header)
                .contentLength(file.length())
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(resource);
    }

    @PutMapping(value = "/{id}", consumes = "application/json")
    public ResponseEntity<DocumentDTO> update(@RequestBody DocumentDTO documentDTO, @PathVariable("id") Long id) {
        Document document = _documentService.findById(id);
        document.setName(documentDTO.getName());
        _documentService.save(document);

        return new ResponseEntity<>(documentDTO, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        Document document = _documentService.findById(id);
        if (document != null) {
            _documentService.findById(document.getId());
            document.setDeleted(true);
            _documentService.save(document);
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
