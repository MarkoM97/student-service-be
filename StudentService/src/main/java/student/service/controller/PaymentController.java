package student.service.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import student.service.dto.PaymentDTO;
import student.service.mappers.PaymentMapper;
import student.service.model.Payment;
import student.service.model.User;
import student.service.service.PaymentService;
import student.service.service.StudentService;
import student.service.service.UserService;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.function.Function;

@RestController
@RequestMapping(value = "/api/payments")
public class PaymentController {

	 @Autowired
	    private PaymentService _paymentService;

	@Autowired
	private UserService _userService;

	@Autowired
	private StudentService _studentService;

	    @GetMapping
	    public ResponseEntity<Page<PaymentDTO>> findAll(@RequestParam(value = "page", defaultValue = "1") int page,
														@RequestParam(value = "size", defaultValue = "2") int size)
		{
			Pageable pageableRequest = PageRequest.of(page - 1, size);
	        Page<Payment> payments = _paymentService.findAll(pageableRequest);
			Page<PaymentDTO> paymentsDTO = PaymentMapper.pageableToDtos(payments);

	        return new ResponseEntity<>(paymentsDTO, HttpStatus.OK);
	    }

		@GetMapping(value = "/student/{id}")
		public ResponseEntity<Page<PaymentDTO>> findAllByStudentId(@RequestParam(value = "page", defaultValue = "1") int page,
																   @RequestParam(value = "size", defaultValue = "2") int size,
																   @PathVariable("id") long id)
		{
			Pageable pageableRequest = PageRequest.of(page - 1, size);
			User user = _userService.findById(id);
			if(user != null){
				if(user.getStudent() != null){
					Page<Payment> payments = _paymentService.findAllByStudentId(user.getStudent().getId(), pageableRequest);
					Page<PaymentDTO> paymentsDTO = PaymentMapper.pageableToDtos(payments);
					return new ResponseEntity<>(paymentsDTO, HttpStatus.OK);
				}
				return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
			}
			return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);

		}

	    @GetMapping(value = "/{id}")
	    public ResponseEntity<PaymentDTO> findById(@PathVariable("id") long id) {
	        Payment payment = _paymentService.findById(id);
	        if(payment == null)
	            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	        PaymentDTO paymentDTO = PaymentMapper.toDto(payment);
	        return new ResponseEntity<>(paymentDTO, HttpStatus.OK);
	    }

	    @PostMapping(consumes = "application/json")
	    public ResponseEntity<PaymentDTO> create(@RequestBody PaymentDTO paymentDTO) {
	    	User user = _userService.findById(paymentDTO.getStudentId());
	    	if(user != null){
	    		if(user.getStudent() != null){
					Payment payment = PaymentMapper.toEntity(paymentDTO);
					payment.setDeleted(false);
					payment.setDate(new Date());
					payment.setStudent(user.getStudent());
					payment = _paymentService.save(payment);
					paymentDTO.setId(payment.getId());
					user.getStudent().setBalance(user.getStudent().getBalance() + paymentDTO.getAmount());
					_studentService.save(user.getStudent());

					return new ResponseEntity<>(paymentDTO, HttpStatus.CREATED);
				}
				return new ResponseEntity<>(new PaymentDTO(), HttpStatus.BAD_REQUEST);
			}
			return new ResponseEntity<>(new PaymentDTO(), HttpStatus.BAD_REQUEST);
	    }

	    @PutMapping(value = "/{id}", consumes = "application/json")
	    public ResponseEntity<PaymentDTO> update(@RequestBody PaymentDTO paymentDTO, @PathVariable("id") long id) {
	        Payment payment = _paymentService.findById(id);
	        if(payment == null)
	            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	        
	        payment.setAmount(paymentDTO.getAmount());
	        payment.setDate(paymentDTO.getDate());
	        payment.setDescription(paymentDTO.getDescription());
	        _paymentService.save(payment);

	        return new ResponseEntity<>(paymentDTO, HttpStatus.OK);
	    }

	    @DeleteMapping(value = "/{id}")
		@PreAuthorize("hasRole('ROLE_ADMINISTRATOR')")
	    public ResponseEntity<Void> delete(@PathVariable("id") long id) {
	        Payment payment = _paymentService.findById(id);
	        if (payment != null) {
	            _paymentService.remove(id);
	            return new ResponseEntity<>(HttpStatus.OK);
	        } else {
	            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	        }
	    }
	
}
