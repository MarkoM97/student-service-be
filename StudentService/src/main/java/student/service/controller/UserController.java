package student.service.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import student.service.dto.*;
import student.service.dto.hybrids.UserHybridDTO;
import student.service.dto.hybrids.UserStudentDTO;
import student.service.dto.hybrids.UserTeacherDTO;
import student.service.mappers.StudentMapper;
import student.service.mappers.TeacherMapper;
import student.service.mappers.UserMapper;
import student.service.model.*;
import student.service.security.TokenUtils;
import student.service.service.*;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value="/api/users")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @Autowired
    TokenUtils tokenUtils;

    @Autowired
    StudentService studentService;

    @Autowired
    private TeacherService _teacherService;

    @Autowired
    private StudyProgramService _studyProgramService;

    @Autowired
    private CourseAttendingService _courseAttendingService;

    @Autowired
    private UserAuthorityService _userAuthorityService;

    @Autowired
    private AuthorityService _authorityService;

    @Autowired
    private PasswordEncoder passwordEncoder;

//http://localhost:8080/api/users?page=0&limit=5
    @GetMapping
    public ResponseEntity<Page<UserDTO>> findAll(@RequestParam(value = "page", defaultValue = "1") int page,
                                              @RequestParam(value = "size", defaultValue = "5") int size)
    {
        Pageable pageableRequest = PageRequest.of(page - 1 , size);
        Page<User> users = userService.findAll(pageableRequest);
        Page<UserDTO> usersDTO = UserMapper.pageableToDtos(users);
        return new ResponseEntity<>(usersDTO, HttpStatus.OK);

    }

    @GetMapping(value="/administrators")
    public ResponseEntity<Page<UserDTO>> findAdministrators(@RequestParam(value = "page", defaultValue = "1") int page,
                                                            @RequestParam(value = "size", defaultValue = "5") int size)
    {
        Pageable pageableRequest = PageRequest.of(page - 1, size);
        Page<User> administrators = _userAuthorityService.findAdministrators(pageableRequest).map(x -> x.getUser());
        Page<UserDTO> usersDTO = UserMapper.pageableToDtos(administrators);
        return new ResponseEntity<>(usersDTO, HttpStatus.OK);

    }

    @GetMapping(value="/{id}")
    public ResponseEntity<UserDTO> findById(@PathVariable("id") Long id)
    {
        User user = userService.findById(id);
        UserDTO userDTO = UserMapper.toDto(user);
        return new ResponseEntity<>(userDTO, HttpStatus.OK);

    }

    @GetMapping(value="without-teacher-student")
    public ResponseEntity<List<UserDTO>> findUserWithoutTeacherAndStudent()
    {
        List<User> users = userService.findByTeacherAndStudent(null, null);
        List<UserDTO> userDTOs = UserMapper.toDtos(users);
        return new ResponseEntity<>(userDTOs, HttpStatus.OK);

    }

    @PostMapping(consumes="application/json")
    public ResponseEntity<UserDTO> create (@RequestBody UserDTO userDTO){
        User user = UserMapper.toEntity(userDTO);
        user.setStudent(studentService.getLast());
        user = userService.save(user);
        userDTO.setId(user.getId());
        return new ResponseEntity<>(userDTO,HttpStatus.CREATED);
    }

    @PostMapping(value = "/create",consumes = "application/json")
    public ResponseEntity<StudentDTO> createUserStudent(@RequestBody UserStudentDTO dto) {
        Student student = new Student(null, dto.getCardNumber(), dto.getAccountNumber(),
                dto.getModelNumber(), dto.getBalance(),
                false, null, null, null, null);
        student = studentService.save(student);

        CourseAttending courseAttending = new CourseAttending();
        courseAttending.setDateOfEnrollment(new Date());
        courseAttending.setDeleted(false);
        courseAttending.setStudent(student);
        courseAttending.setStudyProgram(_studyProgramService.findById(dto.getStudyProgram()));
        _courseAttendingService.save(courseAttending);
        StudentDTO studentDTO = StudentMapper.toDto(student);

        System.out.println(dto.getBirthday());
        User user = new User(null, dto.getUsername(), dto.getFirstname(), dto.getLastname(),
                dto.getBirthday(), dto.getEmail(), dto.getPassword(), dto.getAddress(), false,
                null, student, null);

        if(dto.getBirthday() == null){
            user.setBirthday(new Date());
        }
        user.setPassword(passwordEncoder.encode(dto.getPassword()));
        user = userService.save(user);

        Authority authority = _authorityService.findByName("STUDENT");
        UserAuthority userAuthority = new UserAuthority(user, authority);
        _userAuthorityService.save(userAuthority);

        return new ResponseEntity<>(studentDTO, HttpStatus.CREATED);
    }

    @PostMapping(value = "/create-teacher",consumes = "application/json")
    public ResponseEntity<TeacherDTO> createUserTeacher(@RequestBody UserTeacherDTO dto) {
        Teacher teacher = new Teacher(dto.getTeacherCode(),false, new TeacherType(dto.getTeacherTypeDTO()), null);
        teacher = _teacherService.save(teacher);

        TeacherDTO teacherDTO= TeacherMapper.toDto(teacher);

        User user = new User(null, dto.getUsername(), dto.getFirstname(), dto.getLastname(),
                dto.getBirthday(), dto.getEmail(), dto.getPassword(), dto.getAddress(), dto.getDeleted(),
                teacher, null, null);
        user.setPassword(passwordEncoder.encode(dto.getPassword()));
        user = userService.save(user);

        Authority authority = _authorityService.findByName("TEACHER");
        UserAuthority userAuthority = new UserAuthority(user, authority);
        _userAuthorityService.save(userAuthority);

        return new ResponseEntity<>(teacherDTO, HttpStatus.CREATED);
    }

    @PostMapping(value = "/create-admin",consumes = "application/json")
    public ResponseEntity<UserDTO> createUserAdmin(@RequestBody UserDTO dto) {
        User user = new User(null, dto.getUsername(), dto.getFirstname(), dto.getLastname(),
                dto.getBirthday(), dto.getEmail(), dto.getPassword(), dto.getAddress(), dto.getDeleted(),
                null, null, null);
        if(dto.getBirthday() == null){
            user.setBirthday(new Date());
        }
        user.setPassword(passwordEncoder.encode(dto.getPassword()));
        user.setDeleted(false);
        user = userService.save(user);

        dto.setId(user.getId());

        Authority authority = _authorityService.findByName("ADMINISTRATOR");
        UserAuthority userAuthority = new UserAuthority(user, authority);
        _userAuthorityService.save(userAuthority);

        return new ResponseEntity<>(dto, HttpStatus.CREATED);
    }

//    @PreAuthorize("hasRole('ROLE_ADMINISTRATOR')")
    @PutMapping(value="/{id}",consumes="application/json")
    public ResponseEntity<UserDTO> update (@RequestBody UserDTO userDTO, @PathVariable("id") Long id){
        User user = userService.findById(id);
        user = UserMapper.toEntity(userDTO);

        userService.save(user);
        return new ResponseEntity<>(userDTO,HttpStatus.OK);
    }

    @DeleteMapping(value="/{id}")
    //@PreAuthorize("hasRole('ROLE_ADMINISTRATOR')")
    public ResponseEntity<Void> delete(@PathVariable Long id){
        User user = userService.findById(id);
        if(user != null) {
            userService.deleteAdministrator(user);
            return new ResponseEntity<>(HttpStatus.OK);
        }else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<TokenDTO> login(@RequestBody LoginDTO loginDTO) {
        try {
            UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(
                    loginDTO.getUsername(), loginDTO.getPassword());
            Authentication authentication = authenticationManager.authenticate(token);
            UserDetails details = userDetailsService.loadUserByUsername(loginDTO.getUsername());
            return new ResponseEntity<TokenDTO>(new TokenDTO(tokenUtils.generateToken(details)), HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<TokenDTO>(new TokenDTO(), HttpStatus.BAD_REQUEST);
        }
    }
}
