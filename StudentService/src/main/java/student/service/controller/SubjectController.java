package student.service.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import student.service.dto.ExamGradeDTO;
import student.service.dto.RemainingCoursesDTO;
import student.service.dto.SubjectDTO;
import student.service.mappers.ExamGradeMapper;
import student.service.mappers.StudyProgramMapper;
import student.service.mappers.SubjectMapper;
import student.service.mappers.TeacherMapper;
import student.service.model.*;
import student.service.service.CourseAttendingService;
import student.service.service.ExamGradeService;
import student.service.service.SubjectService;
import student.service.service.UserService;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value="/api/subjects")
public class SubjectController {

    @Autowired
    private SubjectService _subjectService;

    @Autowired
    private UserService _userService;

    @Autowired
    private CourseAttendingService _courseAttendingService;

    @Autowired
    private ExamGradeService _examGradeService;

    @GetMapping
    public ResponseEntity<Page<SubjectDTO>> findAll(@RequestParam(value = "page", defaultValue = "1") int page,
                                                    @RequestParam(value = "size", defaultValue = "2") int size)
    {
        Pageable pageableRequest = PageRequest.of(page - 1, size);
        Page<Subject> subjects = _subjectService.findAllPageable(pageableRequest);
        Page<SubjectDTO> subjectsDTO = SubjectMapper.pageableToDtos(subjects);
        return new ResponseEntity<>(subjectsDTO, HttpStatus.OK);
    }

    @GetMapping(value="/all")
    public ResponseEntity<List<SubjectDTO>> findAll()
    {
        List<Subject> subjects = _subjectService.findAll();
        List<SubjectDTO> subjectsDTO = SubjectMapper.toDtos(subjects);
        return new ResponseEntity<>(subjectsDTO, HttpStatus.OK);
    }

    @GetMapping(value="/{id}")
    public ResponseEntity<SubjectDTO> findById(@PathVariable("id") Long id)
    {
        Subject subject = _subjectService.findById(id);
        SubjectDTO subjectDTO = new SubjectDTO(subject);
        return new ResponseEntity<>(subjectDTO, HttpStatus.OK);
    }


    @GetMapping(value="/remaining-courses/{userId}")
    public ResponseEntity<List<RemainingCoursesDTO>> findRemainingCourses(@PathVariable("userId") Long userId)
    {
        List<User> teachers = new ArrayList<>();

        //find studentId by passed userId
        Long studentId = _userService.findById(userId).getStudent().getId();

        //find course attendings for student
        CourseAttending courseAttending = _courseAttendingService.findByStudentId(studentId);

        //find all subjects that student attend
        List<Subject> subjects = _subjectService.findByStudyProgram(courseAttending.getStudyProgram().getId());

        //find teachers for all subjects
        subjects.stream().forEach( subject -> teachers.add(
                _userService.findByTeacher(subject.getTeacher().getTeacherCode())));

        //Get teachers first name and last name
        List<String> teachersNames = teachers.stream().map(teacher -> new String
                (teacher.getFirstname() + " " + teacher.getLastname()))
                .collect(Collectors.toList());

        //find all passed exam grades for student
        List<ExamGrade> examGrades = _examGradeService.findByStudentPassed(studentId);

        //remove passed subjects
        for (ExamGrade examGrade: examGrades) {
            for (Iterator<Subject> it = subjects.iterator(); it.hasNext();) {
                Subject s = it.next();
                if (examGrade.getExam().getSubject().getId() == s.getId()){
                    it.remove();
                }
            }
        }
        List<RemainingCoursesDTO> remainingCoursesDTOS = RemainingCoursesDTO.toDTOs(subjects, teachersNames);
        return new ResponseEntity<>(remainingCoursesDTOS, HttpStatus.OK);
    }

    @PostMapping(consumes = "application/json")
    public ResponseEntity<SubjectDTO> create(@RequestBody SubjectDTO subjectDTO)
    {
        Subject subject = SubjectMapper.toEntity(subjectDTO);
        subject = _subjectService.save(subject);
        subjectDTO.setId(subject.getId());

        return new ResponseEntity<>(subjectDTO, HttpStatus.OK);
    }

    @PutMapping(value = "/{id}", consumes = "application/json")
    public ResponseEntity<SubjectDTO> update(@RequestBody SubjectDTO subjectDTO, @PathVariable("id") Long id) {
        Subject subject = _subjectService.findById(id);
        if(subject == null)
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        subject.setName(subjectDTO.getName());
        subject.setSubjectCode(subjectDTO.getSubjectCode());
        _subjectService.save(subject);

        return new ResponseEntity<>(subjectDTO, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    @PreAuthorize("hasRole('ROLE_ADMINISTRATOR')")
    public ResponseEntity<Void> delete(@PathVariable("id") Long id) {
        Subject subject = subject = _subjectService.findById(id);
        if (subject != null) {
            _subjectService.remove(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}
