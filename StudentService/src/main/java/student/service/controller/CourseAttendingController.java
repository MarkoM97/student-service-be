package student.service.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import student.service.dto.CourseAttendingDTO;
import student.service.mappers.CourseAttendingMapper;
import student.service.model.CourseAttending;
import student.service.service.CourseAttendingService;
import org.springframework.data.domain.Pageable;

import java.util.List;

@RestController
@RequestMapping(value = "/api/course-attendings")
public class CourseAttendingController {

    @Autowired
    private CourseAttendingService _courseAttendingService;

    @GetMapping
    public ResponseEntity<Page<CourseAttendingDTO>> findAll(@RequestParam(value = "page", defaultValue = "1") int page,
                                                            @RequestParam(value = "size", defaultValue = "2") int size)
    {
        Pageable pageableRequest = PageRequest.of(page - 1, size);
        Page<CourseAttending> courseAttendings = _courseAttendingService.findAll(pageableRequest);
        Page<CourseAttendingDTO> courseAttendingsDTO = CourseAttendingMapper.pageableToDtos(courseAttendings);

        return new ResponseEntity<>(courseAttendingsDTO, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<CourseAttendingDTO> findById(@PathVariable("id") Long id) {
        CourseAttending courseAttending = _courseAttendingService.findById(id);
        CourseAttendingDTO courseAttendingDTO = CourseAttendingMapper.toDto(courseAttending);

        return new ResponseEntity<>(courseAttendingDTO, HttpStatus.OK);
    }

    @PostMapping(consumes = "application/json")
    public ResponseEntity<CourseAttendingDTO> create(@RequestBody CourseAttendingDTO courseAttendingDTO) {
        CourseAttending courseAttending = CourseAttendingMapper.toEntity(courseAttendingDTO);
        courseAttending = _courseAttendingService.save(courseAttending);
        courseAttendingDTO.setId(courseAttending.getId());

        return new ResponseEntity<>(courseAttendingDTO, HttpStatus.CREATED);
    }

    //@PreAuthorize("hasRole('ROLE_ADMINISTRATOR') or hasRole('ROLE_TEACHER')") ?
    @PutMapping(value = "/{id}", consumes = "application/json")
    public ResponseEntity<CourseAttendingDTO> update(@RequestBody CourseAttendingDTO courseAttendingDTO, @PathVariable("id") Long id) {
        CourseAttending courseAttending = _courseAttendingService.findById(id);
        _courseAttendingService.save(courseAttending);

        return new ResponseEntity<>(courseAttendingDTO, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    @PreAuthorize("hasRole('ROLE_ADMINISTRATOR')")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        CourseAttending courseAttending = _courseAttendingService.findById(id);
        if (courseAttending != null) {
            _courseAttendingService.remove(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
