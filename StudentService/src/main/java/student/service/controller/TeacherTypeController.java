package student.service.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import student.service.dto.TeacherTypeDTO;
import student.service.mappers.TeacherMapper;
import student.service.mappers.TeacherTypeMapper;
import student.service.model.TeacherType;
import student.service.service.TeacherTypeService;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value="/api/teacher-type")
public class TeacherTypeController {

    @Autowired
    TeacherTypeService teacherTypeService;

    @GetMapping
    public ResponseEntity<List<TeacherTypeDTO>> findAll()
    {
        List<TeacherType> teacherTypes = teacherTypeService.findAll();
        List<TeacherTypeDTO> teacherTypeDTOS = TeacherTypeMapper.toDtos(teacherTypes);
        return new ResponseEntity<>(teacherTypeDTOS, HttpStatus.OK);
    }

    @GetMapping(value="/{id}")
    public ResponseEntity<TeacherTypeDTO> findById(@PathVariable("id") Long id)
    {
        TeacherType teacherType = teacherTypeService.findById(id);
        TeacherTypeDTO teacherTypeDTO = new TeacherTypeDTO(teacherType);
        return new ResponseEntity<>(teacherTypeDTO, HttpStatus.OK);
    }

    @PostMapping(consumes = "application/json")
    public ResponseEntity<TeacherTypeDTO> create(@RequestBody TeacherTypeDTO teacherTypeDTO)
    {
        TeacherType teacherType = TeacherTypeMapper.toEntity(teacherTypeDTO);
        teacherType = teacherTypeService.save(teacherType);
        teacherTypeDTO.setId(teacherType.getId());

        return new ResponseEntity<>(teacherTypeDTO, HttpStatus.OK);
    }

    @PutMapping(value = "/{id}", consumes = "application/json")
    public ResponseEntity<TeacherTypeDTO> update(@RequestBody TeacherTypeDTO teacherTypeDTO, @PathVariable("id") Long id) {
        TeacherType teacherType = teacherTypeService.findById(id);
        if(teacherType == null)
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        teacherType.setName(teacherTypeDTO.getName());
        teacherTypeService.save(teacherType);

        return new ResponseEntity<>(teacherTypeDTO, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    @PreAuthorize("hasRole('ROLE_ADMINISTRATOR')")
    public ResponseEntity<Void> delete(@PathVariable("id") Long id) {
        TeacherType teacherType = teacherTypeService.findById(id);
        if (teacherType != null) {
            teacherTypeService.remove(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}
