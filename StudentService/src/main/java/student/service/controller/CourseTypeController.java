package student.service.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import student.service.dto.CourseTypeDTO;
import student.service.mappers.CourseTypeMapper;
import student.service.model.CourseType;
import student.service.service.CourseTypeService;

@RestController
@RequestMapping(value = "/api/course-types")
public class CourseTypeController {

    @Autowired
    private CourseTypeService _courseTypeService;

    @GetMapping
    public ResponseEntity<Page<CourseTypeDTO>> findAll(@RequestParam(value = "page", defaultValue = "1") int page,
                                                       @RequestParam(value = "size", defaultValue = "2") int size) {
        Pageable pageableRequest = PageRequest.of(page - 1, size);
        Page<CourseType> courseTypes = _courseTypeService.findAll(pageableRequest);
        Page<CourseTypeDTO> courseTypesDTO = CourseTypeMapper.pageableToDtos(courseTypes);

        return new ResponseEntity<>(courseTypesDTO, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<CourseTypeDTO> findById(@PathVariable("id") Long id) {
        CourseType courseType = _courseTypeService.findById(id);
        CourseTypeDTO courseTypeDTO = CourseTypeMapper.toDto(courseType);

        return new ResponseEntity<>(courseTypeDTO, HttpStatus.OK);
    }

    @PostMapping(consumes = "application/json")
    public ResponseEntity<CourseTypeDTO> create(@RequestBody CourseTypeDTO courseTypeDTO) {
        CourseType courseType = CourseTypeMapper.toEntity(courseTypeDTO);
        courseType.setDeleted(false);
        courseType = _courseTypeService.save(courseType);
        courseTypeDTO.setId(courseType.getId());

        return new ResponseEntity<>(courseTypeDTO, HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}", consumes = "application/json")
    //@PreAuthorize("hasRole('ROLE_ADMINISTRATOR')")
    public ResponseEntity<CourseTypeDTO> update(@RequestBody CourseTypeDTO courseTypeDTO, @PathVariable("id") Long id) {
        CourseType courseType = _courseTypeService.findById(id);
        courseType.setName(courseTypeDTO.getName());

        _courseTypeService.save(courseType);
        return new ResponseEntity<>(courseTypeDTO, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    //@PreAuthorize("hasRole('ROLE_ADMINISTRATOR')")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        CourseType courseType = _courseTypeService.findById(id);
        if (courseType != null) {
            _courseTypeService.remove(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
