package student.service.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import student.service.dto.StudyProgramDTO;
import student.service.mappers.StudyProgramMapper;
import student.service.model.StudyProgram;
import student.service.service.StudyProgramService;

@RestController
@RequestMapping(value = "/api/study-programs")
public class StudyProgramController {

    @Autowired
    private StudyProgramService _studyProgramService;

    @GetMapping
    public ResponseEntity<Page<StudyProgramDTO>> findAll(@RequestParam(value = "page", defaultValue = "1") int page,
                                                         @RequestParam(value = "size", defaultValue = "2") int size) {
        Pageable pageableRequest = PageRequest.of(page - 1, size);
        Page<StudyProgram> studyPrograms = _studyProgramService.findAll(pageableRequest);
        Page<StudyProgramDTO> studyProgramsDTO = StudyProgramMapper.pageableToDtos(studyPrograms);

        return new ResponseEntity<>(studyProgramsDTO, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<StudyProgramDTO> findById(@PathVariable("id") Long id) {
        StudyProgram studyProgram = _studyProgramService.findById(id);
        StudyProgramDTO studyProgramDTO = StudyProgramMapper.toDto(studyProgram);

        return new ResponseEntity<>(studyProgramDTO, HttpStatus.OK);
    }

    @PostMapping(consumes = "application/json")
    public ResponseEntity<StudyProgramDTO> create(@RequestBody StudyProgramDTO studyProgramDTO) {
        StudyProgram studyProgram = StudyProgramMapper.toEntity(studyProgramDTO);
        studyProgram = _studyProgramService.save(studyProgram);
        studyProgramDTO.setId(studyProgram.getId());

        return new ResponseEntity<>(studyProgramDTO, HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}", consumes = "application/json")
    public ResponseEntity<StudyProgramDTO> update(@RequestBody StudyProgramDTO studyProgramDTO, @PathVariable("id") Long id) {
        StudyProgram studyProgram = _studyProgramService.findById(id);
        studyProgram.setName(studyProgramDTO.getName());
        studyProgram.setDuration(studyProgramDTO.getDuration());
        _studyProgramService.save(studyProgram);

        return new ResponseEntity<>(studyProgramDTO, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    //@PreAuthorize("hasRole('ROLE_ADMINISTRATOR')")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        StudyProgram studyProgram = _studyProgramService.findById(id);
        if (studyProgram != null) {
            _studyProgramService.remove(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
