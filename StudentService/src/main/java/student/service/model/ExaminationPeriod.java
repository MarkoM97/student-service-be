package student.service.model;


import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

/***********************************************************************
 * Module:  ExaminationPeriod.java
 * Author:  Bulat
 * Purpose: Defines the Class ExaminationPeriod
 ***********************************************************************/

@Entity
@Table(name="examination_period")
public class ExaminationPeriod implements Serializable {
   @Id
   @GeneratedValue(strategy=GenerationType.IDENTITY)
   @Column(name="examination_period_id", unique=true, nullable=false)
   private Long id;
   private String name;
   private Date startDate;
   private Date endDate;
   private Boolean deleted;

   @OneToMany(cascade = {CascadeType.REFRESH}, fetch = FetchType.LAZY, mappedBy = "examinationPeriod")
   private List<Exam> exams;

   public ExaminationPeriod() {
   }

   public ExaminationPeriod(Long id, String name, Date startDate, Date endDate, Boolean deleted, List<Exam> exams) {
      this.id = id;
      this.name = name;
      this.startDate = startDate;
      this.endDate = endDate;
      this.exams = exams;
      this.deleted = deleted;
   }

   public Long getId() {
      return id;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public Date getStartDate() {
      return startDate;
   }

   public void setStartDate(Date startDate) {
      this.startDate = startDate;
   }

   public Date getEndDate() {
      return endDate;
   }

   public void setEndDate(Date endDate) {
      this.endDate = endDate;
   }

   public List<Exam> getExams() {
      return exams;
   }

   public void setExams(List<Exam> exams) {
      this.exams = exams;
   }

   public Boolean getDeleted() {
      return deleted;
   }

   public void setDeleted(Boolean deleted) {
      this.deleted = deleted;
   }
}