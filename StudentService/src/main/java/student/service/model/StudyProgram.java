package student.service.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

/***********************************************************************
* Module:  StudyProgram.java
* Author:  Bulat
* Purpose: Defines the Class StudyProgram
***********************************************************************/

@Entity
@Table(name="study_program")
public class StudyProgram implements Serializable{
   @Id
   @GeneratedValue(strategy=GenerationType.IDENTITY)
   @Column(name="study_program_id", unique=true, nullable=false)
   private Long id;
   private String name;
   private int duration;
   private Boolean deleted;

   @ManyToOne
   @JoinColumn(name="course_type_id", referencedColumnName="course_type_id", nullable=false)
   private CourseType courseType;

   /*@OneToMany(cascade = {CascadeType.REFRESH}, fetch = FetchType.LAZY, mappedBy = "studyProgram")
   private List<Student> students;*/

   public StudyProgram() {
   }

   public StudyProgram(Long id, String name, int duration, Boolean deleted, CourseType courseType) {
      this.id = id;
      this.name = name;
      this.duration = duration;
      this.courseType = courseType;
      //this.students = students;
      this.deleted = deleted;
   }

   public Long getId() {
      return id;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public int getDuration() {
      return duration;
   }

   public void setDuration(int duration) {
      this.duration = duration;
   }

   public CourseType getCourseType() {
      return courseType;
   }

   public void setCourseType(CourseType courseType) {
      this.courseType = courseType;
   }

   /*public List<Student> getStudents() {
      return students;
   }

   public void setStudents(List<Student> students) {
      this.students = students;
   }*/

   public Boolean getDeleted() {
      return deleted;
   }

   public void setDeleted(Boolean deleted) {
      this.deleted = deleted;
   }


}