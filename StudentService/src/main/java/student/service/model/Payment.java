package student.service.model;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Date;

/***********************************************************************
 * Module:  Payment.java
 * Author:  Bulat
 * Purpose: Defines the Class Payment
 ***********************************************************************/

@Entity
@Table(name="payment")
public class Payment implements Serializable {
   @Id
   @GeneratedValue(strategy=GenerationType.IDENTITY)
   @Column(name="payment_id", unique=true, nullable=false)
   private Long id;
   private String description;
   private Date date;
   private Float amount;
   private Boolean deleted;

   @ManyToOne
   @JoinColumn(name="student_id", referencedColumnName="student_id", nullable=false)
   private Student student;

   public Payment() {
   }

   public Payment(Long id, String description, Date date, Float amount,Boolean deleted, Student student) {
      this.id = id;
      this.description = description;
      this.date = date;
      this.amount = amount;
      this.student = student;
      this.deleted = deleted;
   }

   public Long getId() {
      return id;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public String getDescription() {
      return description;
   }

   public void setDescription(String description) {
      this.description = description;
   }

   public Student getStudent() {
      return student;
   }

   public void setStudent(Student student) {
      this.student = student;
   }

   public Boolean getDeleted() {
      return deleted;
   }

   public void setDeleted(Boolean deleted) {
      this.deleted = deleted;
   }

   public Date getDate() {
      return date;
   }

   public void setDate(Date date) {
      this.date = date;
   }

   public Float getAmount() {
      return amount;
   }

   public void setAmount(Float amount) {
      this.amount = amount;
   }
}