package student.service.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

/***********************************************************************
 * Module:  Exam.java
 * Author:  Bulat
 * Purpose: Defines the Class Exam
 ***********************************************************************/

@Entity
@Table(name="exam")
public class Exam implements Serializable {
   @Id
   @GeneratedValue(strategy=GenerationType.IDENTITY)
   @Column(name="exam_id", unique=true, nullable=false)
   private Long id;
   private Date date;
   private String time;
   private String classRoom;
   private Boolean deleted;

   @ManyToOne
   @JoinColumn(name="teacher_code", referencedColumnName="teacher_code", nullable=false)
   private Teacher teacher;

   @ManyToOne
   @JoinColumn(name="examination_period_id", referencedColumnName="examination_period_id", nullable=false)
   private ExaminationPeriod examinationPeriod;

   @ManyToOne
   @JoinColumn(name="subject_id", referencedColumnName="subject_id", nullable=false)
   private Subject subject;

   @OneToMany(cascade = {CascadeType.REFRESH}, fetch = FetchType.LAZY, mappedBy = "exam")
   private List<ExamGrade> examGrades;

   public Exam() {
   }

   public Exam(Long id, Date date, String time, String classRoom, Boolean deleted, Teacher teacher,
               ExaminationPeriod examinationPeriod, Subject subject, List<ExamGrade> examGrades) {
      this.id = id;
      this.date = date;
      this.time = time;
      this.classRoom = classRoom;
      this.teacher = teacher;
      this.examinationPeriod = examinationPeriod;
      this.subject = subject;
      this.examGrades = examGrades;
      this.deleted = deleted;
   }

   public Long getId() {
      return id;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public Date getDate() {
      return date;
   }

   public void setDate(Date date) {
      this.date = date;
   }

   public String getTime() {
      return time;
   }

   public void setTime(String time) {
      this.time = time;
   }

   public String getClassRoom() {
      return classRoom;
   }

   public void setClassRoom(String classRoom) {
      this.classRoom = classRoom;
   }

   public Teacher getTeacher() {
      return teacher;
   }

   public void setTeacher(Teacher teacher) {
      this.teacher = teacher;
   }

   public ExaminationPeriod getExaminationPeriod() {
      return examinationPeriod;
   }

   public void setExaminationPeriod(ExaminationPeriod examinationPeriod) {
      this.examinationPeriod = examinationPeriod;
   }

   public Subject getSubject() {
      return subject;
   }

   public void setSubject(Subject subject) {
      this.subject = subject;
   }

   public List<ExamGrade> getExamGrades() {
      return examGrades;
   }

   public void setExamGrades(List<ExamGrade> examGrades) {
      this.examGrades = examGrades;
   }

   public Boolean getDeleted() {
      return deleted;
   }

   public void setDeleted(Boolean deleted) {
      this.deleted = deleted;
   }
}