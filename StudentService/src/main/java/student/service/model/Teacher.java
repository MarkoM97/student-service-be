package student.service.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

/***********************************************************************
 * Module:  Teacher.java
 * Author:  Bulat
 * Purpose: Defines the Class Teacher
 ***********************************************************************/

@Entity
@Table(name="teacher")
public class Teacher implements Serializable{
   @Id
   @GeneratedValue(strategy=GenerationType.IDENTITY)
   @Column(name="teacher_code", unique=true, nullable=false)
   private Long teacherCode;
   private Boolean deleted;

   @ManyToOne
   @JoinColumn(name="teacher_type_id", referencedColumnName="teacher_type_id", nullable=false)
   private TeacherType teacherType;

   @OneToMany(cascade = {CascadeType.REFRESH}, fetch = FetchType.LAZY, mappedBy = "teacher")
   private List<Exam> exams;

   public Teacher() {
   }

   public Teacher(Long teacherCode,Boolean deleted, TeacherType teacherType, List<Exam> exams) {
      this.teacherCode = teacherCode;
      this.teacherType = teacherType;
      this.exams = exams;
      this.deleted = deleted;
   }

   public Long getTeacherCode() {
      return teacherCode;
   }
   

   public void setTeacherCode(Long teacherCode) {
      this.teacherCode = teacherCode;
   }

   public TeacherType getTeacherType() {
      return teacherType;
   }

   public void setTeacherType(TeacherType teacherType) {
      this.teacherType = teacherType;
   }

   public List<Exam> getExams() {
      return exams;
   }

   public void setExams(List<Exam> exams) {
      this.exams = exams;
   }

   public Boolean getDeleted() {
      return deleted;
   }

   public void setDeleted(Boolean deleted) {
      this.deleted = deleted;
   }
}