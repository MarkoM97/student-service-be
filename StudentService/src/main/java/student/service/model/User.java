package student.service.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

/***********************************************************************
 * Module:  User.java
 * Author:  Bulat
 * Purpose: Defines the Class User
 ***********************************************************************/

@Entity
@Table(name="user")
public class User implements Serializable{
   @Id
   @GeneratedValue(strategy=GenerationType.IDENTITY)
   @Column(name="user_id", unique=true, nullable=false)
   private Long id;
   private String username;
   private String firstname;
   private String lastname;
   private Date birthday;
   private String email;
   private String password;
   private String address;
   private Boolean deleted;

   @OneToOne
   @JoinColumn(name="teacher_code", referencedColumnName="teacher_code", nullable=true)
   private Teacher teacher;
   @OneToOne
   @JoinColumn(name="student_id", referencedColumnName="student_id", nullable=true)
   private Student student;

   @OneToMany(mappedBy = "user", fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
   private Set<UserAuthority> userAuthorities = new HashSet<UserAuthority>();

   public User() {
   }

   public User(Long id, String username, String firstname, String lastname, Date birthday, String email,
               String password, String address,Boolean deleted, Teacher teacher, Student student, Set<UserAuthority> userAuthorities) {
      this.id = id;
      this.username = username;
      this.firstname = firstname;
      this.lastname = lastname;
      this.birthday = birthday;
      this.email = email;
      this.password = password;
      this.address = address;
      this.teacher = teacher;
      this.student = student;
      this.deleted = deleted;
      this.userAuthorities = userAuthorities;
   }

   public Long getId() {
      return id;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public String getUsername() {
      return username;
   }

   public void setUsername(String username) {
      this.username = username;
   }

   public String getFirstname() {
      return firstname;
   }

   public void setFirstname(String firstname) {
      this.firstname = firstname;
   }

   public String getLastname() {
      return lastname;
   }

   public void setLastname(String lastname) {
      this.lastname = lastname;
   }

   public Date getBirthday() {
      return birthday;
   }

   public void setBirthday(Date birthday) {
      this.birthday = birthday;
   }

   public String getEmail() {
      return email;
   }

   public void setEmail(String email) {
      this.email = email;
   }

   public String getPassword() {
      return password;
   }

   public void setPassword(String password) {
      this.password = password;
   }

	public String getAddress() {
		return address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}

   public Teacher getTeacher() {
      return teacher;
   }

   public void setTeacher(Teacher teacher) {
      this.teacher = teacher;
   }

   public Student getStudent() {
      return student;
   }

   public void setStudent(Student student) {
      this.student = student;
   }

   public Boolean getDeleted() {
      return deleted;
   }

   public void setDeleted(Boolean deleted) {
      this.deleted = deleted;
   }

   public Set<UserAuthority> getUserAuthorities() {
      return userAuthorities;
   }

   public void setUserAuthorities(Set<UserAuthority> userAuthorities) {
      this.userAuthorities = userAuthorities;
   }
}