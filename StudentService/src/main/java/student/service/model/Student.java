package student.service.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

/***********************************************************************
 * Module:  Student.java
 * Author:  Bulat
 * Purpose: Defines the Class Student
 ***********************************************************************/

@Entity
@Table(name="student")
public class Student implements Serializable{
   @Id
   @GeneratedValue(strategy=GenerationType.IDENTITY)
   @Column(name="student_id", unique=true, nullable=false)
   private Long id;
   private String cardNumber;
   private String accountNumber;
   private String modelNumber;
   private Float balance;
   private Boolean deleted;

   @OneToMany(cascade = {CascadeType.REFRESH}, fetch = FetchType.LAZY, mappedBy = "student")
   private List<CourseAttending> courseAttending;

   @OneToMany(cascade = {CascadeType.REFRESH},fetch = FetchType.LAZY, mappedBy = "student")
   private List<Document> documents;

   @OneToMany(cascade = {CascadeType.REFRESH}, fetch = FetchType.LAZY, mappedBy = "student")
   private List<ExamGrade> examGrades;

   @OneToMany(cascade = {CascadeType.REFRESH},fetch = FetchType.LAZY, mappedBy = "student")
   private List<Payment> payments;

   public Student() {
   }

   public Student(Long id, String cardNumber, String accountNumber, String modelNumber, Float balance,
                  Boolean deleted, List<CourseAttending> courseAttending,
		   List<Document> documents, List<ExamGrade> examGrades, List<Payment> payments) {
      this.id = id;
      this.cardNumber = cardNumber;
      this.accountNumber = accountNumber;
      this.modelNumber = modelNumber;
      this.balance = balance;
      this.courseAttending = courseAttending;
      this.documents = documents;
      this.examGrades = examGrades;
      this.payments = payments;
      this.deleted = deleted;
   }

   public Long getId() {
      return id;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public String getCardNumber() {
      return cardNumber;
   }

   public void setCardNumber(String cardNumber) {
      this.cardNumber = cardNumber;
   }

   public String getAccountNumber() {
      return accountNumber;
   }

   public void setAccountNumber(String accountNumber) {
      this.accountNumber = accountNumber;
   }

   public String getModelNumber() {
      return modelNumber;
   }

   public void setModelNumber(String modelNumber) {
      this.modelNumber = modelNumber;
   }

   public Float getBalance() {
      return balance;
   }

   public void setBalance(Float balance) {
      this.balance = balance;
   }

   public List<CourseAttending> getCourseAttending() {
      return courseAttending;
   }

   public void setCourseAttending(List<CourseAttending> courseAttending) {
      this.courseAttending = courseAttending;
   }

   public List<Document> getDocuments() {
      return documents;
   }

   public void setDocuments(List<Document> documents) {
      this.documents = documents;
   }

   public List<ExamGrade> getExamGrades() {
      return examGrades;
   }

   public void setExamGrades(List<ExamGrade> examGrades) {
      this.examGrades = examGrades;
   }

   public List<Payment> getPayments() {
      return payments;
   }

   public void setPayments(List<Payment> payments) {
      this.payments = payments;
   }

   public Boolean getDeleted() {
      return deleted;
   }

   public void setDeleted(Boolean deleted) {
      this.deleted = deleted;
   }

}