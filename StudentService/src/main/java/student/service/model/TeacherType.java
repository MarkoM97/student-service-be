package student.service.model;

import student.service.dto.TeacherTypeDTO;

import javax.persistence.*;

import java.io.Serializable;

/***********************************************************************
 * Module:  TeacherType.java
 * Author:  Bulat
 * Purpose: Defines the Class TeacherType
 ***********************************************************************/

@Entity
@Table(name="teacher_type")
public class TeacherType implements Serializable{
   @Id
   @GeneratedValue(strategy=GenerationType.IDENTITY)
   @Column(name="teacher_type_id", unique=true, nullable=false)
   private Long id;
   private String name;
   private Boolean deleted;

   public TeacherType() {
   }

   public TeacherType(Long id, String name, Boolean deleted) {
      this.id = id;
      this.name = name;
      this.deleted = deleted;
   }

   public TeacherType(TeacherTypeDTO dto){
      this.id = dto.getId();
      this.name = dto.getName();
      this.deleted = dto.getDeleted();
   }

   public Long getId() {
      return id;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public Boolean getDeleted() {
      return deleted;
   }

   public void setDeleted(Boolean deleted) {
      this.deleted = deleted;
   }
}