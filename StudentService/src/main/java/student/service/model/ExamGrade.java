package student.service.model;

import javax.persistence.*;

import java.io.Serializable;

/***********************************************************************
 * Module:  ExamGrade.java
 * Author:  Bulat
 * Purpose: Defines the Class ExamGrade
 ***********************************************************************/

@Entity
@Table(name="exam_grade")
public class ExamGrade implements Serializable {
   @Id
   @GeneratedValue(strategy=GenerationType.IDENTITY)
   @Column(name="exam_grade_id", unique=true, nullable=false)
   private Long id;
   private Integer grade;
   private Float points;
   private Boolean deleted;

   @ManyToOne
   @JoinColumn(name="student_id", referencedColumnName="student_id", nullable=false)
   private Student student;

   @ManyToOne
   @JoinColumn(name="exam_id", referencedColumnName="exam_id", nullable=false)
   private Exam exam;

   public ExamGrade() {
   }

   public ExamGrade(Long id, Integer grade, Float points, Boolean deleted, Student student, Exam exam) {
      this.id = id;
      this.grade = grade;
      this.points = points;
      this.student = student;
      this.exam = exam;
      this.deleted = deleted;
   }

   public Long getId() {
      return id;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public Integer getGrade() {
      return grade;
   }

   public void setGrade(Integer grade) {
      this.grade = grade;
   }

   public Float getPoints() {
      return points;
   }

   public void setPoints(Float points) {
      this.points = points;
   }

   public Student getStudent() {
      return student;
   }

   public void setStudent(Student student) {
      this.student = student;
   }

   public Exam getExam() {
      return exam;
   }

   public void setExam(Exam exam) {
      this.exam = exam;
   }

   public Boolean getDeleted() {
      return deleted;
   }

   public void setDeleted(Boolean deleted) {
      this.deleted = deleted;
   }
}