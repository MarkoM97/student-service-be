package student.service.model;


import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

/***********************************************************************
* Module:  Subject.java
* Author:  Bulat
* Purpose: Defines the Class Subject
***********************************************************************/

@Entity
@Table(name="subject")
public class Subject implements Serializable{
   @Id
   @GeneratedValue(strategy=GenerationType.IDENTITY)
   @Column(name="subject_id", unique=true, nullable=false)
   private Long id;
   private String name;
   private Long subjectCode;
   private Boolean deleted;

   @ManyToOne
   @JoinColumn(name="study_program_id", referencedColumnName="study_program_id", nullable=false)
   private StudyProgram studyProgram;

   @ManyToOne
   @JoinColumn(name="teacher_code", referencedColumnName="teacher_code", nullable=false)
   private Teacher teacher;


   public Subject() {
   }

   public Subject(Long id, String name, Long subjectCode, Boolean deleted,
                  StudyProgram studyProgram, Teacher teacher) {
      this.id = id;
      this.name = name;
      this.subjectCode = subjectCode;
      this.deleted = deleted;
      this.studyProgram = studyProgram;
      this.teacher = teacher;
   }

   public Subject(Subject subject) {
      this(subject.getId(), subject.getName(), subject.getSubjectCode(), subject.getDeleted(),
              subject.getStudyProgram(), subject.getTeacher());
   }

   public Long getId() {
      return id;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public Long getSubjectCode() {
      return subjectCode;
   }

   public void setSubjectCode(Long subjectCode) {
      this.subjectCode = subjectCode;
   }

   public Boolean getDeleted() {
      return deleted;
   }

   public void setDeleted(Boolean deleted) {
      this.deleted = deleted;
   }

   public StudyProgram getStudyProgram() {
      return studyProgram;
   }

   public void setStudyProgram(StudyProgram studyProgram) {
      this.studyProgram = studyProgram;
   }

   public Teacher getTeacher() { return teacher; }

   public void setTeacher(Teacher teacher) { this.teacher = teacher; }
}