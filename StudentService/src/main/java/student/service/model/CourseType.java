package student.service.model;

import javax.persistence.*;

import java.io.Serializable;

/***********************************************************************
 * Module:  CoursType.java
 * Author:  Bulat
 * Purpose: Defines the Class CoursType
 ***********************************************************************/

@Entity
@Table(name="course_type")
public class CourseType implements Serializable {
   @Id
   @GeneratedValue(strategy=GenerationType.IDENTITY)
   @Column(name="course_type_id", unique=true, nullable=false)
   private Long id;
  // @Column(unique = true, nullable = false)
   private String name;
   private Boolean deleted;

   public CourseType(Long id, String name, Boolean deleted) {
      this.id = id;
      this.name = name;
      this.deleted = deleted;
   }

   public CourseType() {
   }

   @Override
   public boolean equals(Object o) {
      if (this == o)
         return true;
      if (o == null || getClass() != o.getClass())
         return false;
      CourseType courseType = (CourseType) o;
      return id.equals(courseType.id);
   }


   public Long getId() {
      return id;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public Boolean getDeleted() {
      return deleted;
   }

   public void setDeleted(Boolean deleted) {
      this.deleted = deleted;
   }
}