package student.service.model;

import javax.persistence.*;

import java.io.Serializable;

/***********************************************************************
 * Module:  Document.java
 * Author:  Bulat
 * Purpose: Defines the Class Document
 ***********************************************************************/

@Entity
@Table(name="document")
public class Document implements Serializable {
   @Id
   @GeneratedValue(strategy=GenerationType.IDENTITY)
   @Column(name="document_id", unique=true, nullable=false)
   private Long id;
   private String name;
   private Boolean deleted;

   @ManyToOne
   @JoinColumn(name="student_id", referencedColumnName="student_id", nullable=false)
   private Student student;

   public Document() {
      this.deleted = false;
   }

   public Document(Long id, String name, Boolean deleted, Student student) {
      this.id = id;
      this.name = name;
      this.student = student;
      this.deleted = deleted;
   }

   public Long getId() {
      return id;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public Student getStudent() {
      return student;
   }

   public void setStudent(Student student) {
      this.student = student;
   }

   public Boolean getDeleted() {
      return deleted;
   }

   public void setDeleted(Boolean deleted) {
      this.deleted = deleted;
   }
}