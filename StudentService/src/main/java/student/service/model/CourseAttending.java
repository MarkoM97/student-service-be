package student.service.model;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Date;

/***********************************************************************
 * Module:  CourseAttending.java
 * Author:  Bulat
 * Purpose: Defines the Class CourseAttending
 ***********************************************************************/

@Entity
@Table(name="course_attending")
public class CourseAttending implements Serializable {
   @Id
   @GeneratedValue(strategy=GenerationType.IDENTITY)
   @Column(name="course_attending_id", unique=true, nullable=false)
   private Long id;

   private Date dateOfEnrollment;

   @ManyToOne
   @JoinColumn(name="study_program_id", referencedColumnName="study_program_id", nullable=false)
   private StudyProgram studyProgram;

   @ManyToOne
   @JoinColumn(name="student_id", referencedColumnName="student_id", nullable=false)
   private Student student;

   private Boolean deleted;

   public CourseAttending(Long id, Boolean deleted, Date dateOfEnrollment, StudyProgram studyProgram, Student student) {
      this.id = id;
      this.dateOfEnrollment = dateOfEnrollment;
      this.studyProgram = studyProgram;
      this.student = student;
      this.deleted = deleted;
   }

    public CourseAttending() {
    }

    @Override
   public boolean equals(Object o) {
      if (this == o)
         return true;
      if (o == null || getClass() != o.getClass())
         return false;
      CourseAttending courseAttending = (CourseAttending) o;
      return id.equals(courseAttending.id);
   }

   public Long getId() {
      return id;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public StudyProgram getStudyProgram() {
      return studyProgram;
   }

   public void setStudyProgram(StudyProgram studyProgram) {
      this.studyProgram = studyProgram;
   }

   public Student getStudent() {
      return student;
   }

   public void setStudent(Student student) {
      this.student = student;
   }

   public Boolean getDeleted() {
      return deleted;
   }

   public void setDeleted(Boolean deleted) {
      this.deleted = deleted;
   }

   public Date getDateOfEnrollment() {
      return dateOfEnrollment;
   }

   public void setDateOfEnrollment(Date dateOfEnrollment) {
      this.dateOfEnrollment = dateOfEnrollment;
   }
}