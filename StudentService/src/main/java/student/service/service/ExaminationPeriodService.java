package student.service.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;
import student.service.model.ExaminationPeriod;
import student.service.repository.ExaminationPeriodRepository;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

@Service
public class ExaminationPeriodService {

    @Autowired
    ExaminationPeriodRepository repository;

    @Autowired
    ExamService _examService;


    @Query
    public List<ExaminationPeriod> findCurrentPeriod (){
        LocalDate localBefore = LocalDate.now().minusDays(30);
        LocalDate localAfter = LocalDate.now().plusDays(30);
        Date startDateBefore = Date.from(localBefore.atStartOfDay(ZoneId.systemDefault()).toInstant());
        Date startDateAfter = Date.from(localAfter.atStartOfDay(ZoneId.systemDefault()).toInstant());
        return repository.findByEndDateAfterAndStartDateBetweenAndDeletedFalse( new Date(), startDateBefore, startDateAfter);
    }

    public ExaminationPeriod findById(Long id) {
        return repository.findByIdAndDeletedFalse(id);
    }

    public Page<ExaminationPeriod> findAll(Pageable page) {
        return repository.findAllByDeletedFalse(page);
    }

    public ExaminationPeriod save(ExaminationPeriod examinationPeriod) {
        return repository.save(examinationPeriod);
    }

    public void remove(Long id) {
        ExaminationPeriod examinationPeriod = this.findById(id);
        examinationPeriod.setDeleted(true);
        this.save(examinationPeriod);
        _examService.removeByExamPeriod(id);
    }

}
