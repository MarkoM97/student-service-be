package student.service.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import student.service.model.Exam;
import student.service.repository.ExamRepository;

import java.util.List;

@Service
public class ExamService {

    @Autowired
    ExamRepository repository;

    @Autowired
    ExamGradeService _examGradeService;

    public Exam findById(Long id) {
        return repository.findByIdAndDeletedFalse(id);
    }

    public List<Exam> findByExaminationPeriod_Id(Long id){
        return repository.findByExaminationPeriod_IdAndDeletedFalse(id);
    }

    public List<Exam> findbySubject(Long id){
        return repository.findBySubject_IdAndDeletedFalse(id);
    }

    public Page<Exam> findAllPageable(Pageable page) {
        return repository.findAllByDeletedFalse(page);
    }

    public List<Exam> findByTeacher(Long teacherCode){
        return repository.findAllByDeletedFalseAndTeacherTeacherCode(teacherCode);
    }

    public List<Exam> findAll(){
        return repository.findAllByDeletedFalse();
    }

    public Exam save(Exam exam) {
        return repository.save(exam);
    }
    
    public void remove(Long id) {
        Exam exam = this.findById(id);
        exam.setDeleted(true);
        this.save(exam);
        _examGradeService.removeByExam(id);
    }

    public void delete(Exam exam) {
        exam.setDeleted(true);
        this.save(exam);
    }

    public void removeByExamPeriod(Long id) {
        List<Exam> exams = this.findByExaminationPeriod_Id(id);
        exams.stream().forEach(x -> this.delete(x));
        _examGradeService.removeByExam(id);
    }

    public void removeBySubject(Long id){
        List<Exam> exams = this.findbySubject(id);
        exams.stream().forEach(x -> this.delete(x));
        _examGradeService.removeByExam(id);
    }

}
