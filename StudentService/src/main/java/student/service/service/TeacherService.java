package student.service.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import student.service.model.Teacher;
import student.service.repository.TeacherRepository;

import java.util.List;

@Service
public class TeacherService {

    @Autowired
    TeacherRepository repository;

    public Teacher findByTeacherCode(Long teacherCode) {
        return repository.findByTeacherCodeAndDeletedFalse(teacherCode);
    }

    public Page<Teacher> findAll(Pageable page) {
        return repository.findAllByDeletedFalse(page);
    }

    public Teacher save(Teacher teacher) {
        return repository.save(teacher);
    }
/*public Teacher Create(Teacher teacher, Long userId) {
        Teacher createdTeacher = repository.save(teacher);
        User user = userRepository.findById(userId);
        user.setTeacher(teacher);
        userRepository.save(user);
        return repository.save(teacher);
    }
    public Teacher Update(Teacher teacher) {
        return repository.save(teacher);
    }*/
    public void remove(Long teacherCode) {
        Teacher teacher = this.findByTeacherCode(teacherCode);
        teacher.setDeleted(true);
        this.save(teacher);
    }

}
