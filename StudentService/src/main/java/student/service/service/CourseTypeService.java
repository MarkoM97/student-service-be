package student.service.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import student.service.model.CourseType;
import student.service.repository.CourseTypeRepository;

import java.util.List;

@Service
public class CourseTypeService {

    @Autowired
    CourseTypeRepository repository;

    @Autowired
    StudyProgramService _studyProgramService;

    public CourseType findById(Long id) {
        return repository.findByIdAndDeletedFalse(id);
    }

    public Page<CourseType> findAll(Pageable page) {
        return repository.findAllByDeletedFalse(page);
    }

    public CourseType save(CourseType courseType) {
        return repository.save(courseType);
    }

    public void remove(Long id) {
        CourseType courseType = this.findById(id);
        courseType.setDeleted(true);
        this.save(courseType);
        _studyProgramService.removeByCourseType(id);
    }

}
