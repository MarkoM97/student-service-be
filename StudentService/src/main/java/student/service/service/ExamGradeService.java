package student.service.service;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import student.service.model.ExamGrade;
import student.service.repository.ExamGradeRepository;

import java.util.ArrayList;
import java.util.List;


@Service
public class ExamGradeService {

    @Autowired
    ExamGradeRepository repository;

    public ExamGrade findById(Long id) {
        return repository.findByIdAndDeletedFalse(id);
    }

    public List<ExamGrade> findByExamId(Long examId){
        return repository.findByExam_IdAndDeletedFalse(examId);
    }

    public List<ExamGrade> findByStudentPassed(Long studentId){
        return  repository.findByStudent_IdAndDeletedFalseAndGradeGreaterThanEqual(studentId, 6);
    }

    public List<ExamGrade> findByStudent(Long studentId){
        return  repository.findByStudent_IdAndDeletedFalseAndGradeGreaterThanEqual(studentId, 5);
    }

    public List<ExamGrade> findAllByStudent(Long studentId){
        return  repository.findByStudent_IdAndDeletedFalseAndGradeGreaterThanEqual(studentId, 0);
    }

    public List<ExamGrade> findByStudentRegistered(Long studentId){ return  repository.findByStudent_IdAndDeletedFalseAndGrade(studentId, 0);}

    public Page<ExamGrade> findAll(Pageable page) {
        return repository.findAllByDeletedFalse(page);
    }

    public ExamGrade save(ExamGrade examGrade) {
        return repository.save(examGrade);
    }

    public List<ExamGrade> saveAll(List<ExamGrade> examGrades) {
        List<ExamGrade> results = new ArrayList<>();
        for (ExamGrade eg : examGrades) {
            results.add(repository.save(eg));
        }

        return results;
    }

    public void remove(Long id) {
        ExamGrade examGrade = this.findById(id);
        examGrade.setDeleted(true);
        this.save(examGrade);
    }

    public void delete(ExamGrade examGrade) {
        examGrade.setDeleted(true);
        this.save(examGrade);
    }

    public void removeByStudent(Long studentId) {
        List<ExamGrade> examGrades = this.findAllByStudent(studentId);
        examGrades.stream().forEach(x -> this.delete(x));
    }

    public void removeByExam(Long examId) {
        List<ExamGrade> examGrades = this.findByExamId(examId);
        examGrades.stream().forEach(x -> this.delete(x));
    }

}
