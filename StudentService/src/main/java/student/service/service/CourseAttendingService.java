package student.service.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import student.service.model.CourseAttending;
import student.service.repository.CourseAttendingRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class CourseAttendingService {

    @Autowired
    CourseAttendingRepository repository;

    public CourseAttending findById(Long id) {
        return repository.findByIdAndDeletedFalse(id);
    }

    public CourseAttending findByStudentId(Long id) {
        return repository.findByStudent_IdAndDeletedFalse(id);
    }

    public Page<CourseAttending> findAll(Pageable page) {
        return repository.findAllByDeletedFalse(page);
    }

    public CourseAttending save(CourseAttending courseAttending) {
        return repository.save(courseAttending);
    }

    public List<CourseAttending> saveAll(List<CourseAttending> courseAttendings) {
        List<CourseAttending> result = new ArrayList<>();
        courseAttendings.stream().forEach(courseAttending -> result.add(repository.save(courseAttending)));
        return  result;
    }

    public void remove(Long id) {
        CourseAttending courseAttending = this.findById(id);
        courseAttending.setDeleted(true);
        this.save(courseAttending);
    }

}
