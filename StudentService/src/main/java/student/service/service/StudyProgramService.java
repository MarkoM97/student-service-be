package student.service.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import student.service.model.StudyProgram;
import student.service.repository.StudyProgramRepository;

import java.util.List;

@Service
public class StudyProgramService {

    @Autowired
    StudyProgramRepository repository;

    @Autowired
    SubjectService _subjectService;

    public StudyProgram findById(Long id) {
        return repository.findByIdAndDeletedFalse(id);
    }

    public StudyProgram findByName(String name) {
        return repository.findByNameAndDeletedFalse(name);
    }

    public Page<StudyProgram> findAll(Pageable page) {
        return repository.findAllByDeletedFalse(page);
    }

    public List<StudyProgram> findByCourseType(Long id) {
        return repository.findByCourseType_IdAndDeletedFalse(id);
    }

    public StudyProgram save(StudyProgram studyProgram) {
        return repository.save(studyProgram);
    }

    public void remove(Long id) {
        StudyProgram studyProgram = this.findById(id);
        studyProgram.setDeleted(true);
        this.save(studyProgram);
    }

    public void delete(StudyProgram studyProgram) {
        studyProgram.setDeleted(true);
        this.save(studyProgram);
        _subjectService.removeByStudyProgram(studyProgram.getId());
    }

    public void removeByCourseType(Long id){
        List<StudyProgram> studyPrograms = this.findByCourseType(id);
        studyPrograms.stream().forEach(x -> this.delete(x));
    }

}
