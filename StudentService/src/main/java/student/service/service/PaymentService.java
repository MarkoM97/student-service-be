package student.service.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import student.service.model.Payment;
import student.service.repository.PaymentRepository;

import java.util.List;

@Service
public class PaymentService {

    @Autowired
    PaymentRepository repository;

    public Payment findById(Long id) {
        return repository.findByIdAndDeletedFalse(id);
    }

    public Page<Payment> findAll(Pageable page) {
        return repository.findAllByDeletedFalse(page);
    }

    public Page<Payment> findAllByStudentId(Long studentId, Pageable page) {return repository.findAllByStudentIdAndDeletedFalse(studentId, page);}

    public Payment save(Payment payment) {
        return repository.save(payment);
    }

    public void remove(Long id) {
        Payment payment = this.findById(id);
        payment.setDeleted(true);
        this.save(payment);
    }

}
