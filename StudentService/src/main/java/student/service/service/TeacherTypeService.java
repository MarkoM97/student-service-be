package student.service.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import student.service.model.TeacherType;
import student.service.repository.TeacherTypeRepository;

import java.util.List;

@Service
public class TeacherTypeService {

    @Autowired
    TeacherTypeRepository repository;

    public TeacherType findById(Long id) {
        return repository.findByIdAndDeletedFalse(id);
    }

    public TeacherType findByName(String name) {
        return repository.findByNameAndDeletedFalse(name);
    }

    public List<TeacherType> findAll() {
        return repository.findAllByDeletedFalse();
    }

    public TeacherType save(TeacherType teacherType) {
        return repository.save(teacherType);
    }

    public void remove(Long id) {
        TeacherType teacherType = this.findById(id);
        teacherType.setDeleted(true);
        this.save(teacherType);
    }

}
