package student.service.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import student.service.model.Student;
import student.service.model.Teacher;
import student.service.model.User;
import student.service.repository.UserRepository;

import java.util.List;

@Service
public class UserService {

    @Autowired
    UserRepository repository;

    @Autowired
    TeacherService _teacherService;

    @Autowired
    StudentService _studentService;


    public Page<User> findAdministrator(Pageable pageable){
        return repository.findByDeletedFalseAndStudent_IdAndTeacher_TeacherCode(pageable, null, null);
    }

    public User findByUsername(String username) {
        return repository.findByUsernameAndDeletedFalse(username);
    }

    public User findByStudent(Long id) {
        return repository.findByStudent_IdAndDeletedFalse(id);
    }

    public User findByTeacher(Long id) {
        return repository.findByTeacher_TeacherCodeAndDeletedFalse(id);
    }

    public User findById(Long id) {
        return repository.findByIdAndDeletedFalse(id);
    }

    public Page<User> findAll(Pageable page) {
        return repository.findAllByDeletedFalse(page);
    }

    public List<User> findByTeacherAndStudent(Long teacherId, Long studentId) {

        return repository.findByTeacherAndStudentAndDeletedFalse(teacherId, studentId);
    }

    public User save(User user) {
        return repository.save(user);
    }

    public void remove(Long id) {
        User user = this.findById(id);
        user.setDeleted(true);
        this.save(user);
    }

    public void deleteAdministrator(User user){
        user.setDeleted(true);
        save(user);
    }

    public void deleteStudent(User user){
        user.setDeleted(true);
        save(user);
        _studentService.remove(user.getStudent().getId());
    }

    public void deleteTeacher(User user){
        user.setDeleted(true);
        save(user);
        _teacherService.remove(user.getTeacher().getTeacherCode());

    }
}
