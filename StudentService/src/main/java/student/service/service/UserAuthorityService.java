package student.service.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import student.service.model.UserAuthority;
import student.service.repository.UserAuthorityRepository;

import java.util.List;

@Service
public class UserAuthorityService {
    @Autowired
    private UserAuthorityRepository repository;

    public Page<UserAuthority> findByAuthority(String authorityName, Pageable pageable){
        return repository.findByAuthority_NameAndUser_Deleted(authorityName, false, pageable);
    }

    public Page<UserAuthority> findTeachers(Pageable pageable){
        return repository.findByAuthority_NameAndUser_Deleted("TEACHER", false, pageable);
    }


    public Page<UserAuthority> findAdministrators(Pageable pageable){
        return repository.findByAuthority_NameAndUser_Deleted("ADMINISTRATOR", false, pageable);
    }

    public Page<UserAuthority> findStudents(Pageable pageable){
        return repository.findByAuthority_NameAndUser_Deleted("STUDENT", false, pageable);
    }

    public UserAuthority findById(Long id) {
        return repository.getOne(id);
    }

    public Page<UserAuthority> findAll(Pageable page) {
        return repository.findAll(page);
    }

    public UserAuthority save(UserAuthority userAuthority) {
        return repository.save(userAuthority);
    }

    public void remove(Long id) {
        repository.deleteById(id);
    }
}