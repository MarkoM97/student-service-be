package student.service.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import student.service.model.Authority;
import student.service.repository.AuthorityRepository;

@Service
public class AuthorityService {

    @Autowired
    AuthorityRepository repository;

    public Authority findById(Long id) {
        return repository.getOne(id);
    }
    public Authority findByName(String name) {
        return repository.findByName(name);
    }

    public Page<Authority> findAll(Pageable page) {
        return repository.findAll(page);
    }

    public Authority save(Authority authority) {
        return repository.save(authority);
    }

    public void remove(Long id) {
        repository.deleteById(id);
    }
}
