package student.service.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import student.service.model.Student;
import student.service.repository.StudentRepository;

import java.util.List;

@Service
public class StudentService {

    @Autowired
    StudentRepository repository;

    @Autowired
    ExamGradeService _examGradeService;

    public Student findById(Long id) {
        return repository.findByIdAndDeletedFalse(id);
    }

    public Student getById(Long id) {
        return repository.findById(id).orElse(null);
    }

    public Student findByCardNumber(String cardNumber) {
        return repository.findByCardNumberAndDeletedFalse(cardNumber);
    }

    public Page<Student> findAll(Pageable page) {
        return repository.findAllByDeletedFalse(page);
    }

    public Student save(Student student) {
        return repository.save(student);
    }
    
    public void remove(long id) {
        Student student = this.findById(id);
        student.setDeleted(true);
        _examGradeService.removeByStudent(id);
        this.save(student);
    }

    public void removeByCardNumber(String cardNumber) {
        repository.deleteByCardNumber(cardNumber);
    }

    public Student getLast() {
        return repository.findFirstByDeletedFalseOrderByIdDesc();
    }

}
