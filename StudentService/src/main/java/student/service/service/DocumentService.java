package student.service.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import student.service.model.Document;
import student.service.repository.DocumentRepository;

import java.util.List;


@Service
public class DocumentService {

    @Autowired
    DocumentRepository repository;

    public Document findById(Long id) {
        return repository.getOne(id);
    }

    public Document getById(Long id) {return repository.findById(id).orElse(null);}

    public List<Document> findByStudentId(Long id) { return repository.findByStudent_IdAndDeletedFalse(id); }

    public Page<Document> findAll(Pageable page) {
        return repository.findAll(page);
    }

    public Document save(Document document) {
        return repository.save(document);
    }

    public void remove(Long id) {
        Document document = this.findById(id);
        document.setDeleted(true);
        this.save(document);
    }

}
