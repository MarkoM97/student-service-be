package student.service.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import student.service.model.Subject;
import student.service.repository.SubjectRepository;

import java.util.List;

@Service
public class SubjectService {

    @Autowired
    SubjectRepository repository;

    @Autowired
    ExamService _examService;

    public List<Subject> findByTeacher(Long teacherCode){
        return  repository.findByTeacher_TeacherCodeAndDeletedFalse(teacherCode);
    }

    public Subject findById(Long id) {
        return repository.findByIdAndDeletedFalse(id);
    }

    public Page<Subject> findAllPageable(Pageable page) {
        return repository.findAllByDeletedFalse(page);
    }
    public List<Subject> findAll() {
        return repository.findAll();
    }
    public List<Subject> findByStudyProgram(Long id) {
        return repository.findByStudyProgram_IdAndDeletedFalse(id);
    }

    public Subject save(Subject subject) {
        return repository.save(subject);
    }

    public void remove(Long id) {
        Subject subject = this.findById(id);
        subject.setDeleted(true);
        this.save(subject);
    }
    public void delete(Subject subject) {
        subject.setDeleted(true);
        this.save(subject);
        _examService.removeBySubject(subject.getId());
    }

    public void removeByStudyProgram(Long id){
        List<Subject> subjects = findByStudyProgram(id);
        subjects.stream().forEach(x -> this.delete(x));
    }

}
