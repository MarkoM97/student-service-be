package student.service.mappers;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;
import student.service.dto.StudyProgramDTO;
import student.service.model.StudyProgram;

public class StudyProgramMapper {

	public static StudyProgramDTO toDto(StudyProgram studyProgram) {
		return new StudyProgramDTO(studyProgram);
	}
	
	public static StudyProgram toEntity(StudyProgramDTO dto) {
		return new StudyProgram(dto.getId(), dto.getName(), dto.getDuration(), dto.getDeleted(),
				    CourseTypeMapper.toEntity(dto.getCourseType()));
	}
	
	public static List<StudyProgramDTO> toDtos(List<StudyProgram> entities) {
		List<StudyProgramDTO> dtos = entities.stream().map(x -> new StudyProgramDTO(x))
				.collect(Collectors.toList());
		return dtos;
	}
	
	public static List<StudyProgram> toEntities(List<StudyProgramDTO> dtos) {
		List<StudyProgram> entities = dtos.stream().map(x -> new StudyProgram(x.getId(), x.getName(),
				x.getDuration(), x.getDeleted(),CourseTypeMapper.toEntity(x.getCourseType())))
				.collect(Collectors.toList());
		return entities;
	}

	public static Page<StudyProgramDTO> pageableToDtos(Page<StudyProgram> entities) {
		Page<StudyProgramDTO> dtos = entities.map(x -> new StudyProgramDTO(x));
		return dtos;
	}
}
