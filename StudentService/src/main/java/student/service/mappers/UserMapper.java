package student.service.mappers;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;
import student.service.dto.UserDTO;
import student.service.model.User;

public class UserMapper {

	public static UserDTO toDto(User user) {
		return new UserDTO(user);
	}
	
	public static User toEntity(UserDTO dto) {
		return new User(dto.getId(), dto.getUsername(), dto.getFirstname(), dto.getLastname(),
				dto.getBirthday(), dto.getEmail(), dto.getPassword(), dto.getAddress(), dto.getDeleted(),
				null, null, null);
	}
	
	public static List<UserDTO> toDtos(List<User> entities) {
		List<UserDTO> dtos = entities.stream().map(x -> new UserDTO(x))
                .collect(Collectors.toList());
		return dtos;
	}
	
	public static List<User> toEntities(List<UserDTO> dtos) {
		List<User> entities = dtos.stream().map(x -> new User(x.getId(), x.getUsername(), x.getFirstname(),
                x.getLastname(), x.getBirthday(), x.getEmail(), x.getPassword(), x.getAddress(), x.getDeleted(),
                null, null, null))
                .collect(Collectors.toList());
		return entities;
	}

	public static Page<UserDTO> pageableToDtos(Page<User> entities) {
		Page<UserDTO> dtos = entities.map(x -> new UserDTO(x));
		return dtos;
	}
}
