package student.service.mappers;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;
import student.service.dto.TeacherTypeDTO;
import student.service.model.TeacherType;

public class TeacherTypeMapper {

	public static TeacherTypeDTO toDto(TeacherType TeacherType) {
		return new TeacherTypeDTO(TeacherType);
	}
	
	public static TeacherType toEntity(TeacherTypeDTO dto) {
		return new TeacherType(dto.getId(), dto.getName(), dto.getDeleted());
	}
	
	public static List<TeacherTypeDTO> toDtos(List<TeacherType> entities) {
		List<TeacherTypeDTO> dtos = entities.stream().map(x -> new TeacherTypeDTO(x))
				.collect(Collectors.toList());
		return dtos;
	}
	
	public static List<TeacherType> toEntities(List<TeacherTypeDTO> dtos) {
		List<TeacherType> entities = dtos.stream().map(x -> new TeacherType(x.getId(), x.getName(), x.getDeleted()))
				.collect(Collectors.toList());
		return entities;
	}

	public static Page<TeacherTypeDTO> pageableToDtos(Page<TeacherType> entities) {
		Page<TeacherTypeDTO> dtos = entities.map(x -> new TeacherTypeDTO(x));
		return dtos;
	}
}
