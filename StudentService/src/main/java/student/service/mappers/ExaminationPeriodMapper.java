package student.service.mappers;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;
import student.service.dto.ExaminationPeriodDTO;
import student.service.model.ExaminationPeriod;

public class ExaminationPeriodMapper {

	public static ExaminationPeriodDTO toDto(ExaminationPeriod examinationPeriod) {
		return new ExaminationPeriodDTO(examinationPeriod);
	}
	
	public static ExaminationPeriod toEntity(ExaminationPeriodDTO dto) {
		return new ExaminationPeriod(dto.getId(), dto.getName(),
				   dto.getStartDate(), dto.getEndDate(), dto.getDeleted(),null);
	}
	
	public static List<ExaminationPeriodDTO> toDtos(List<ExaminationPeriod> entities) {
		List<ExaminationPeriodDTO> dtos = entities.stream().map(x -> new ExaminationPeriodDTO(x))
				.collect(Collectors.toList());
		return dtos;
	}
	
	public static List<ExaminationPeriod> toEntities(List<ExaminationPeriodDTO> dtos) {
		List<ExaminationPeriod> entities = dtos.stream().map(x -> new ExaminationPeriod(x.getId(),
				x.getName(), x.getStartDate(), x.getEndDate(), x.getDeleted(), null))
				.collect(Collectors.toList());
		return entities;
	}

	public static Page<ExaminationPeriodDTO> pageableToDtos(Page<ExaminationPeriod> entities) {
		Page<ExaminationPeriodDTO> dtos = entities.map(x -> new ExaminationPeriodDTO(x));
		return dtos;
	}
}
