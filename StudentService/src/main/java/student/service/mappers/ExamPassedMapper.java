package student.service.mappers;

import student.service.dto.ExamPassedDTO;
import student.service.model.ExamGrade;

import java.util.ArrayList;
import java.util.List;

public class ExamPassedMapper {

    public static List<ExamPassedDTO> toDTOs(List<ExamGrade> examGrades, List<String> teacherNames){
        List<ExamPassedDTO> examPassedDTOS = new ArrayList<>();
        for (int i = 0; examGrades.size() > i; i++){
            ExamPassedDTO examPassedDTO = new ExamPassedDTO(examGrades.get(i),teacherNames.get(i));
            examPassedDTOS.add(examPassedDTO);
        }
        return examPassedDTOS;
    }
}
