package student.service.mappers;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;
import student.service.dto.DocumentDTO;
import student.service.model.Document;

public class DocumentMapper {

	public static DocumentDTO toDto(Document document) {
		return new DocumentDTO(document);
	}
	
	public static Document toEntity(DocumentDTO dto) {
		return new Document(dto.getId(), dto.getName(), dto.getDeleted(), null);
	}
	
	public static List<DocumentDTO> toDtos(List<Document> entities) {
		List<DocumentDTO> dtos = entities.stream().map(x -> new DocumentDTO(x))
				.collect(Collectors.toList());
		return dtos;
	}
	
	public static List<Document> toEntities(List<DocumentDTO> dtos) {
		List<Document> entities = dtos.stream().map(x -> new Document(x.getId(), x.getName(), x.getDeleted(),
				null))
				.collect(Collectors.toList());
		return entities;
	}

	public static Page<DocumentDTO> pageableToDtos(Page<Document> entities) {
		Page<DocumentDTO> dtos = entities.map(x -> new DocumentDTO(x));
		return dtos;
	}
}
