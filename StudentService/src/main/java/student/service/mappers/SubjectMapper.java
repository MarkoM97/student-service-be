package student.service.mappers;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;
import student.service.dto.SubjectDTO;
import student.service.model.Subject;

public class SubjectMapper {

	public static SubjectDTO toDto(Subject subject) {
		return new SubjectDTO(subject);
	}
	
	public static Subject toEntity(SubjectDTO dto) {
		return new Subject(dto.getId(), dto.getName(), dto.getSubjectCode(), dto.getDeleted(),
				   null, null);
	}
	
	public static List<SubjectDTO> toDtos(List<Subject> entities) {
		List<SubjectDTO> dtos = entities.stream().map(x -> new SubjectDTO(x))
				.collect(Collectors.toList());
		return dtos;
	}
	
	public static List<Subject> toEntities(List<SubjectDTO> dtos) {
		List<Subject> entities = dtos.stream().map(x -> new Subject(x.getId(), x.getName(), x.getSubjectCode(),
				x.getDeleted(), null, null))
				.collect(Collectors.toList());
		return entities;
	}

	public static Page<SubjectDTO> pageableToDtos(Page<Subject> entities) {
		Page<SubjectDTO> dtos = entities.map(x -> new SubjectDTO(x));
		return dtos;
	}
}
