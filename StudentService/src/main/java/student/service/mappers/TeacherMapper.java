package student.service.mappers;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;
import student.service.dto.TeacherDTO;
import student.service.model.Teacher;

public class TeacherMapper {

	public static TeacherDTO toDto(Teacher teacher) {
		return new TeacherDTO(teacher);
	}
	
	public static Teacher toEntity(TeacherDTO dto) {
		return new Teacher(dto.getTeacherCode(), dto.getDeleted(), null, null);
	}
	
	public static List<TeacherDTO> toDtos(List<Teacher> entities) {
		List<TeacherDTO> dtos = entities.stream().map(x -> new TeacherDTO(x))
				.collect(Collectors.toList());
		return dtos;
	}
	
	public static List<Teacher> toEntities(List<TeacherDTO> dtos) {
		List<Teacher> entities = dtos.stream().map(x -> new Teacher(x.getTeacherCode(), x.getDeleted(),
				null, null))
				.collect(Collectors.toList());
		return entities;
	}

	public static Page<TeacherDTO> pageableToDtos(Page<Teacher> entities) {
		Page<TeacherDTO> dtos = entities.map(x -> new TeacherDTO(x));
		return dtos;
	}
}
