package student.service.mappers;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;
import student.service.dto.PaymentDTO;
import student.service.model.Payment;

public class PaymentMapper {

	public static PaymentDTO toDto(Payment payment) {
		return new PaymentDTO(payment);
	}
	
	public static Payment toEntity(PaymentDTO dto) {
		return new Payment(dto.getId(), dto.getDescription(), dto.getDate(),
				   dto.getAmount(), dto.getDeleted(), null);
	}
	
	public static List<PaymentDTO> toDtos(List<Payment> entities) {
		List<PaymentDTO> dtos = entities.stream().map(x -> new PaymentDTO(x))
				.collect(Collectors.toList());
		return dtos;
	}
	
	public static List<Payment> toEntities(List<PaymentDTO> dtos) {
		List<Payment> entities = dtos.stream().map(x -> new Payment(x.getId(), x.getDescription(),
				x.getDate(), x.getAmount(), x.getDeleted(), null))
				.collect(Collectors.toList());
		return entities;
	}

	public static Page<PaymentDTO> pageableToDtos(Page<Payment> entities) {
		Page<PaymentDTO> dtos = entities.map(x -> new PaymentDTO(x));
		return dtos;
	}
}
