package student.service.mappers;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;
import student.service.dto.StudentDTO;
import student.service.model.Student;

public class StudentMapper {

	public static StudentDTO toDto(Student student) {
		return new StudentDTO(student);
	}
	
	public static Student toEntity(StudentDTO dto) {
		return new Student(dto.getId() ,dto.getCardNumber(), dto.getAccountNumber(), dto.getModelNumber(),
				   dto.getBalance(), dto.getDeleted(),
				   null,null,null,null);
	}
	
	public static List<StudentDTO> toDtos(List<Student> entities) {
		List<StudentDTO> dtos = entities.stream().map(x -> new StudentDTO(x))
				.collect(Collectors.toList());
		return dtos;
	}
	
	public static List<Student> toEntities(List<StudentDTO> dtos) {
		List<Student> entities = dtos.stream().map(x -> new Student(x.getId(), x.getCardNumber(),
				x.getAccountNumber(), x.getModelNumber(), x.getBalance(), x.getDeleted(),
				null,null,null,null))
				.collect(Collectors.toList());
		return entities;
	}

	public static Page<StudentDTO> pageableToDtos(Page<Student> entities) {
		Page<StudentDTO> dtos = entities.map(x -> new StudentDTO(x));
		return dtos;
	}
}
