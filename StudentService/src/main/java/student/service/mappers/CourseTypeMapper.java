package student.service.mappers;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;
import student.service.dto.CourseTypeDTO;
import student.service.model.CourseType;
public class CourseTypeMapper {

	public static CourseTypeDTO toDto(CourseType entity) {
		return new CourseTypeDTO(entity);
	}
	
	public static CourseType toEntity(CourseTypeDTO dto) {
		return new CourseType(dto.getId(),dto.getName(), dto.getDeleted());
	}
	
	public static List<CourseTypeDTO> toDtos(List<CourseType> entities) {
		List<CourseTypeDTO> dtos = entities.stream().map(x -> new CourseTypeDTO(x))
				.collect(Collectors.toList());
		return dtos;
	}
	
	public static List<CourseType> toEntities(List<CourseTypeDTO> dtos) {
		List<CourseType> entities = dtos.stream().map(x -> new CourseType(x.getId(), x.getName(), x.getDeleted()))
				.collect(Collectors.toList());
		return entities;
	}

	public static Page<CourseTypeDTO> pageableToDtos(Page<CourseType> entities) {
		Page<CourseTypeDTO> dtos = entities.map(x -> new CourseTypeDTO(x));
		return dtos;
	}
}
