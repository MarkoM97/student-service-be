package student.service.mappers;

import org.springframework.data.domain.Page;
import student.service.dto.ExamDetailsDTO;
import student.service.model.Exam;

import java.util.List;
import java.util.stream.Collectors;

public class ExamDetailsMapper {
    public static ExamDetailsDTO toDto(Exam exam) {
        return new ExamDetailsDTO(exam);
    }

    public static Exam toEntity(ExamDetailsDTO dto) {
        return new Exam(dto.getId(), dto.getDate(), dto.getTime(), dto.getClassRoom(), dto.getDeleted(),
                null, null, null, null);
    }

    public static List<ExamDetailsDTO> toDtos(List<Exam> entities) {
        List<ExamDetailsDTO> dtos = entities.stream().map(x -> new ExamDetailsDTO(x))
                .collect(Collectors.toList());
        return dtos;
    }

    public static List<Exam> toEntities(List<ExamDetailsDTO> dtos) {
        List<Exam> entities = dtos.stream().map(x -> new Exam(x.getId(), x.getDate(), x.getTime(),
                x.getClassRoom(), x.getDeleted(), null, null, null, null))
                .collect(Collectors.toList());
        return entities;
    }

    public static Page<ExamDetailsDTO> pageableToDtos(Page<Exam> entities) {
        Page<ExamDetailsDTO> dtos = entities.map(x -> new ExamDetailsDTO(x));
        return dtos;
    }
}
