package student.service.mappers;

import student.service.dto.hybrids.TeacherExamDTO;
import student.service.model.Exam;

import java.util.List;
import java.util.stream.Collectors;

public class TeacherExamMapper {

    public static List<TeacherExamDTO> toDTOs(List<Exam> exams){
        List<TeacherExamDTO> dtos = exams.stream().map(exam -> new TeacherExamDTO(exam)).collect(Collectors.toList());
        return dtos;
    }
}
