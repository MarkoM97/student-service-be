package student.service.mappers;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;
import student.service.dto.CourseAttendingDTO;
import student.service.model.CourseAttending;

public class CourseAttendingMapper {

	public static CourseAttendingDTO toDto(CourseAttending entity) {
		return new CourseAttendingDTO(entity);
	}
	
	public static CourseAttending toEntity(CourseAttendingDTO dto) {
		return new CourseAttending(dto.getId(), dto.getDeleted(), dto.getDateOfEnrollment(),null, null);
	}
	
	public static List<CourseAttendingDTO> toDtos(List<CourseAttending> entities) {
		List<CourseAttendingDTO> dtos = entities.stream().map(x -> new CourseAttendingDTO(x))
				.collect(Collectors.toList());
		return dtos;
	}
	
	public static List<CourseAttending> toEntities(List<CourseAttendingDTO> dtos) {
		List<CourseAttending> entities = dtos.stream().map(x ->
				new CourseAttending(x.getId(), x.getDeleted(), x.getDateOfEnrollment(),
				null, null))
				.collect(Collectors.toList());
		return entities;
	}

	public static Page<CourseAttendingDTO> pageableToDtos(Page<CourseAttending> entities) {
		Page<CourseAttendingDTO> dtos = entities.map(x -> new CourseAttendingDTO(x));
		return dtos;
	}
}
