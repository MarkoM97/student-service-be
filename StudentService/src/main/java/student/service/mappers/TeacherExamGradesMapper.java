package student.service.mappers;

import student.service.dto.hybrids.TeacherExamGradesDTO;
import student.service.model.Exam;
import student.service.model.ExamGrade;
import student.service.model.Student;
import student.service.model.Teacher;

import java.util.ArrayList;
import java.util.List;

public class TeacherExamGradesMapper {

    public static ExamGrade toEntity(TeacherExamGradesDTO dto) {
        return new ExamGrade(dto.getExamId(), dto.getGrade(), dto.getPoints(), false,
                null, null);
    }

    public static List<TeacherExamGradesDTO> toDtos(List<ExamGrade> examGrades, List<String> studentNames) {
        List<TeacherExamGradesDTO> dtos = new ArrayList<>();
        for (int i = 0; examGrades.size() > i; i++){
            TeacherExamGradesDTO dto = new TeacherExamGradesDTO(examGrades.get(i),studentNames.get(i));
            dtos.add(dto);
        }
        return dtos;
    }

    public static List<ExamGrade> toEntities(List<TeacherExamGradesDTO> examGradesDTOS, List<Student> students, Exam exam) {
        List<ExamGrade> entities = new ArrayList<>();
        for (int i = 0; examGradesDTOS.size() > i; i++){
            ExamGrade entity = new ExamGrade(examGradesDTOS.get(i).getExamGradeId(),
                    examGradesDTOS.get(i).getGrade(), examGradesDTOS.get(i).getPoints(),
                    false,students.get(i), exam);
            entities.add(entity);
        }
        return entities;
    }
}
