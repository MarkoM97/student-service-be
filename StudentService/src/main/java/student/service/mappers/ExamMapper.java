package student.service.mappers;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;
import student.service.dto.*;
import student.service.model.Exam;

public class ExamMapper {

	public static ExamDTO toDto(Exam exam) {
		return new ExamDTO(exam);
	}
	
	public static Exam toEntity(ExamDTO dto) {
		return new Exam(dto.getId(), dto.getDate(), dto.getTime(), dto.getClassRoom(), dto.getDeleted(),
				null, null, null, null);
	}
	
	public static List<ExamDTO> toDtos(List<Exam> entities) {
		List<ExamDTO> dtos = entities.stream().map(x -> new ExamDTO(x))
				.collect(Collectors.toList());
		return dtos;
	}
	
	public static List<Exam> toEntities(List<ExamDTO> dtos) {
		List<Exam> entities = dtos.stream().map(x -> new Exam(x.getId(), x.getDate(), x.getTime(),
				x.getClassRoom(), x.getDeleted(), null, null, null, null))
				.collect(Collectors.toList());
		return entities;
	}

	public static Page<ExamDTO> pageableToDtos(Page<Exam> entities) {
		Page<ExamDTO> dtos = entities.map(x -> new ExamDTO(x));
		return dtos;
	}
}
