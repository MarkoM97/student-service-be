package student.service.mappers;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;
import student.service.dto.ExamGradeDTO;
import student.service.model.ExamGrade;

public class ExamGradeMapper {

	public static ExamGradeDTO toDto(ExamGrade examGrade) {
		return new ExamGradeDTO(examGrade);
	}
	
	public static ExamGrade toEntity(ExamGradeDTO dto) {
		return new ExamGrade(dto.getId(), dto.getGrade(), dto.getPoints(), dto.getDeleted(),
				null, null);
	}
	
	public static List<ExamGradeDTO> toDtos(List<ExamGrade> entities) {
		List<ExamGradeDTO> dtos = entities.stream().map(x -> new ExamGradeDTO(x))
				.collect(Collectors.toList());
		return dtos;
	}
	
	public static List<ExamGrade> toEntities(List<ExamGradeDTO> dtos) {
		List<ExamGrade> entities = dtos.stream().map(x -> new ExamGrade(x.getId(), x.getGrade(), x.getPoints(),
				x.getDeleted(), null, null)).collect(Collectors.toList());
		return entities;
	}

	public static Page<ExamGradeDTO> pageableToDtos(Page<ExamGrade> entities) {
		Page<ExamGradeDTO> dtos = entities.map(x -> new ExamGradeDTO(x));
		return dtos;
	}
}
