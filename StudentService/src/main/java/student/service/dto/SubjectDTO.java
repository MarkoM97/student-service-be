package student.service.dto;

import student.service.model.Subject;

public class SubjectDTO {

    private Long id;
    private String name;
    private Long subjectCode;
    private Boolean deleted;

    public SubjectDTO() {
    }

    public SubjectDTO(Long id, String name, Long subjectCode, Boolean deleted) {
        this.id = id;
        this.name = name;
        this.subjectCode = subjectCode;
        this.deleted = deleted;
    }

    public SubjectDTO(Subject subject) {
        this(subject.getId(), subject.getName(), subject.getSubjectCode(), subject.getDeleted());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getSubjectCode() {
        return subjectCode;
    }

    public void setSubjectCode(Long subjectCode) {
        this.subjectCode = subjectCode;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
