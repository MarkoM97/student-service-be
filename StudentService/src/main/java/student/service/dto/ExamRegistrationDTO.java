package student.service.dto;

public class ExamRegistrationDTO {
    private Long examId;
    private Long studentId;

    public ExamRegistrationDTO(Long examId, Long studentId) {
        this.examId = examId;
        this.studentId = studentId;
    }

    public Long getExamId() {
        return examId;
    }

    public void setExamId(Long examId) {
        this.examId = examId;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public ExamRegistrationDTO() {
    }
}
