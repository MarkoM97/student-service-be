package student.service.dto;

import student.service.model.Exam;

import java.util.Date;

public class ExamDTO {

    private Long id;
    private Date date;
    private String time;
    private String classRoom;
    private Boolean deleted;


    public ExamDTO() {
    }

    public ExamDTO(Long id, Date date, String time, String classRoom, Boolean deleted) {
        this.id = id;
        this.date = date;
        this.time = time;
        this.classRoom = classRoom;
        this.deleted = deleted;
    }

    public ExamDTO(Exam exam) {
        this(exam.getId(), exam.getDate(), exam.getTime(), exam.getClassRoom(), exam.getDeleted());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getClassRoom() {
        return classRoom;
    }

    public void setClassRoom(String classRoom) {
        this.classRoom = classRoom;
    }


    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
