package student.service.dto;

import student.service.model.Teacher;

public class CreateTeacherDTO {
    private long userId;
    private long teacherTypeId;

    public CreateTeacherDTO() {
    }

    public CreateTeacherDTO(long userId, long teacherTypeId) {
        this.userId = userId;
        this.teacherTypeId = teacherTypeId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getTeacherTypeId() {
        return teacherTypeId;
    }

    public void setTeacherTypeId(long teacherTypeId) {
        this.teacherTypeId = teacherTypeId;
    }
}