package student.service.dto;

import student.service.model.ExamGrade;

public class ExamGradeDTO {

    private Long id;
    private Integer grade;
    private Float points;
    private Boolean deleted;

    public ExamGradeDTO() {
    }

    public ExamGradeDTO(Long id, Integer grade, Float points, Boolean deleted) {
        this.id = id;
        this.grade = grade;
        this.points = points;
        this.deleted = deleted;
    }

    public ExamGradeDTO(ExamGrade examGrade) {
        this(examGrade.getId(), examGrade.getGrade(), examGrade.getPoints(), examGrade.getDeleted());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public Float getPoints() {
        return points;
    }

    public void setPoints(Float points) {
        this.points = points;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
