package student.service.dto;

import student.service.model.Subject;

import java.util.ArrayList;
import java.util.List;

public class RemainingCoursesDTO {
    private String studyProgram;
    private String subjectName;
    private String teacherName;

    public RemainingCoursesDTO(String studyProgram, String subjectName, String teacherName) {
        this.studyProgram = studyProgram;
        this.subjectName = subjectName;
        this.teacherName = teacherName;
    }

    public RemainingCoursesDTO(){

    }

    public RemainingCoursesDTO(Subject subject, String teacherName){
        this(subject.getStudyProgram().getName(),subject.getName(), teacherName);
    }

    public static List<RemainingCoursesDTO> toDTOs(List<Subject> subjects, List<String> teacherNames){
        List<RemainingCoursesDTO> remainingCoursesDTOS = new ArrayList<>();
        for (int i = 0; subjects.size() > i; i++){
            RemainingCoursesDTO remainingCoursesDTO = new RemainingCoursesDTO(subjects.get(i),teacherNames.get(i));
            remainingCoursesDTOS.add(remainingCoursesDTO);
        }
        return remainingCoursesDTOS;
    }

    public String getStudyProgram() {
        return studyProgram;
    }

    public void setStudyProgram(String studyProgram) {
        this.studyProgram = studyProgram;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }
}
