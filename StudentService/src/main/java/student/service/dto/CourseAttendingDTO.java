package student.service.dto;

import student.service.model.CourseAttending;

import java.util.Date;

public class CourseAttendingDTO {

    private Long id;
    private Date dateOfEnrollment;
    private Boolean deleted;
    private Long studyProgramId;
    private String studyProgramName;
    private Long studentId;


    public CourseAttendingDTO(Long id, Boolean deleted, Date dateOfEnrollment,
                              Long studyProgramId, String studyProgramName, Long studentId) {
        this.id = id;
        this.deleted = deleted;
        this.dateOfEnrollment = dateOfEnrollment;
        this.studyProgramId = studyProgramId;
        this.studyProgramName = studyProgramName;
        this.studentId = studentId;

    }
    

    public CourseAttendingDTO(CourseAttending courseAttending) {
        this(courseAttending.getId(), courseAttending.getDeleted(), courseAttending.getDateOfEnrollment(),
                courseAttending.getStudyProgram().getId(), courseAttending.getStudyProgram().getName(),
                courseAttending.getStudent().getId());

    }
    
    public CourseAttendingDTO(){

    }

    public Date getDateOfEnrollment() {
        return dateOfEnrollment;
    }

    public void setDateOfEnrollment(Date dateOfEnrollment) {
        this.dateOfEnrollment = dateOfEnrollment;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Long getStudyProgramId() {
        return studyProgramId;
    }

    public void setStudyProgramId(Long studyProgramId) {
        this.studyProgramId = studyProgramId;
    }

    public String getStudyProgramName() {
        return studyProgramName;
    }

    public void setStudyProgramName(String studyProgramName) {
        this.studyProgramName = studyProgramName;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }
}
