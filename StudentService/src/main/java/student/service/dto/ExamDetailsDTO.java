package student.service.dto;

import student.service.model.Exam;

import java.util.Date;

public class ExamDetailsDTO extends ExamDTO {
    private SubjectDTO subject;

    public ExamDetailsDTO() {
    }

    public ExamDetailsDTO(Long id, Date date, String time, String classRoom, Boolean deleted, SubjectDTO subject) {
        super(id, date, time, classRoom, deleted);
        this.subject = subject;
    }

    public ExamDetailsDTO(Exam exam) {
        this(exam.getId(), exam.getDate(), exam.getTime(), exam.getClassRoom(), exam.getDeleted(), new SubjectDTO(exam.getSubject()));
    }

    public SubjectDTO getSubject() {
        return subject;
    }

    public void setSubject(SubjectDTO subject) {
        this.subject = subject;
    }
}
