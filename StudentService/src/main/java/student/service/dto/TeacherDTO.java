package student.service.dto;

import student.service.model.Teacher;

public class TeacherDTO {

    private Long teacherCode;
    private Boolean deleted;
    private TeacherTypeDTO teacherTypeDTO;
    
    public TeacherDTO() {
    }

    public TeacherDTO(Long teacherCode, Boolean deleted, TeacherTypeDTO teacherTypeDTO) {
        this.teacherCode = teacherCode;
        this.deleted = deleted;
        this.teacherTypeDTO = teacherTypeDTO;
    }

    public TeacherDTO(Teacher teacher) {
    	this(teacher.getTeacherCode(), teacher.getDeleted(), new TeacherTypeDTO(
    	        teacher.getTeacherType().getId(), teacher.getTeacherType().getName(), teacher.getTeacherType().getDeleted()));
    }
    
    public Long getTeacherCode() {
        return teacherCode;
    }

    public void setTeacherCode(Long teacherCode) {
        this.teacherCode = teacherCode;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public TeacherTypeDTO getTeacherTypeDTO() {
        return teacherTypeDTO;
    }

    public void setTeacherTypeDTO(TeacherTypeDTO teacherTypeDTO) {
        this.teacherTypeDTO = teacherTypeDTO;
    }
}
