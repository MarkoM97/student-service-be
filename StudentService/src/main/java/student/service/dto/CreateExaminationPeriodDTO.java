package student.service.dto;

import java.util.ArrayList;
import java.util.Date;

public class CreateExaminationPeriodDTO {

    private Long id;
    private String name;
    private Date startDate;
    private Date endDate;
    private Boolean deleted;
    private ArrayList<Long> exams;

    public CreateExaminationPeriodDTO(){}

    public CreateExaminationPeriodDTO(Long id, String name, Date startDate, Date endDate, Boolean deleted, ArrayList<Long> exams) {
        this.id = id;
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.deleted = deleted;
        this.exams = exams;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public ArrayList<Long> getExams() {
        return exams;
    }

    public void setExams(ArrayList<Long> exams) {
        this.exams = exams;
    }
}
