package student.service.dto;

import student.service.model.Student;

public class StudentDTO {

    private Long id;
    private String cardNumber;
    private String accountNumber;
    private String modelNumber;
    private Float balance;
    private Boolean deleted;

    public StudentDTO() {
    }

    public StudentDTO(Long id, String cardNumber, String accountNumber, String modelNumber, Float balance, Boolean deleted) {
        this.id = id;
        this.cardNumber = cardNumber;
        this.accountNumber = accountNumber;
        this.modelNumber = modelNumber;
        this.balance = balance;
        this.deleted = deleted;
    }

    public StudentDTO(Student student) {
        this(student.getId(), student.getCardNumber(), student.getAccountNumber(), student.getModelNumber(),
                student.getBalance(), student.getDeleted());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getModelNumber() {
        return modelNumber;
    }

    public void setModelNumber(String modelNumber) {
        this.modelNumber = modelNumber;
    }

    public Float getBalance() {
        return balance;
    }

    public void setBalance(Float balance) {
        this.balance = balance;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
