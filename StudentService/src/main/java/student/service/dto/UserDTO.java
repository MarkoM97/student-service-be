package student.service.dto;

import student.service.model.User;

import java.util.Date;

public class UserDTO {
    private Long id;
    private String username;
    private String firstname;
    private String lastname;
    private Date birthday;
    private String email;
    private String password;
    private String address;
    private Boolean deleted;

    public UserDTO() {
    }

    public UserDTO(Long id, String username, String firstname, String lastname,
                   Date birthday, String email, String password, String address, Boolean deleted) {
        this.id = id;
        this.username = username;
        this.firstname = firstname;
        this.lastname = lastname;
        this.birthday = birthday;
        this.email = email;
        this.password = password;
        this.address = address;
        this.deleted = deleted;
    }

    public UserDTO(User user) {
        this(user.getId(), user.getUsername(), user.getFirstname(),
                user.getLastname(), user.getBirthday(), user.getEmail(),
                user.getPassword(), user.getAddress(), user.getDeleted());
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
