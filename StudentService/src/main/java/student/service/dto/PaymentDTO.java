package student.service.dto;

import student.service.model.Payment;

import java.util.Date;

public class PaymentDTO {

    private Long id;
    private String description;
    private Date date;
    private Float amount;
    private Boolean deleted;
    private Long studentId;

    public PaymentDTO() {
    }

    public PaymentDTO(Long id, String description, Date date, Float amount, Boolean deleted, Long studentId) {
        this.id = id;
        this.description = description;
        this.date = date;
        this.amount = amount;
        this.deleted = deleted;
        this.studentId = studentId;
    }

    public PaymentDTO(Payment payment) {
        this(payment.getId(), payment.getDescription(), payment.getDate(),
                payment.getAmount(), payment.getDeleted(), payment.getStudent().getId());
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
