package student.service.dto;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import student.service.model.Exam;
import student.service.model.ExaminationPeriod;

public class ExaminationPeriodDTO {

    private Long id;
    private String name;
    private Date startDate;
    private Date endDate;
    private Boolean deleted;
    private List<Long> exams;

   

	public ExaminationPeriodDTO() {
    }

    public ExaminationPeriodDTO(Long id, String name, Date startDate, Date endDate, Boolean deleted, List<Long> exams) {
        this.id = id;
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.deleted = deleted;
        this.exams = exams;
    }
    
    public ExaminationPeriodDTO(ExaminationPeriod examinationPeriod) {
    	this(examinationPeriod.getId(), examinationPeriod.getName(), examinationPeriod.getStartDate(),
                examinationPeriod.getEndDate(), examinationPeriod.getDeleted(), examinationPeriod.getExams().stream().map(x -> x.getId()).collect(Collectors.toList()));
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public List<Long> getExams() {
        return exams;
    }

    public void setExams(List<Long> exams) {
        this.exams = exams;
    }
}
