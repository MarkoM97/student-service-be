package student.service.dto;

import student.service.model.CourseType;

public class CourseTypeDTO {

    private Long id;
    private String name;
    private Boolean deleted;

    public CourseTypeDTO(Long id, String name, Boolean deleted) {
        this.id = id;
        this.name = name;
        this.deleted = deleted;
    }

    public CourseTypeDTO(CourseType courseType) {
    	this(courseType.getId(), courseType.getName(), courseType.getDeleted());
    }
    
    public CourseTypeDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
