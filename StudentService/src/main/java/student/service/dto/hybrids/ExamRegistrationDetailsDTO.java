package student.service.dto.hybrids;

import java.util.List;

public class ExamRegistrationDetailsDTO {

    private String examinationPeriod;
    private List<SubjectRegistrationDetailsDTO> subjects;
    private Float studentBalance;

    public ExamRegistrationDetailsDTO() {
    }

    public ExamRegistrationDetailsDTO(String examinationPeriod, List<SubjectRegistrationDetailsDTO> subjects, Float studentBalance) {
        this.examinationPeriod = examinationPeriod;
        this.subjects = subjects;
        this.studentBalance = studentBalance;
    }

    public Float getStudentBalance() {
        return studentBalance;
    }

    public void setStudentBalance(Float studentBalance) {
        this.studentBalance = studentBalance;
    }

    public String getExaminationPeriod() {
        return examinationPeriod;
    }

    public void setExaminationPeriod(String examinationPeriod) {
        this.examinationPeriod = examinationPeriod;
    }

    public List<SubjectRegistrationDetailsDTO> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<SubjectRegistrationDetailsDTO> subjects) {
        this.subjects = subjects;
    }
}
