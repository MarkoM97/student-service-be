package student.service.dto.hybrids;

import student.service.model.ExamGrade;

public class TeacherExamGradesDTO {

    private Long examGradeId;
    private Long examId;
    private String subjectName;
    private String studentName;
    private Long studentId;
    private Integer grade;
    private Float points;

    public TeacherExamGradesDTO(Long examGradeId, Long examId, String subjectName, String studentName, Long studentId, Integer grade, Float points) {
        this.examId = examId;
        this.examGradeId = examGradeId;
        this.subjectName = subjectName;
        this.studentName = studentName;
        this.studentId = studentId;
        this.grade = grade;
        this.points = points;
    }
    public TeacherExamGradesDTO(ExamGrade examGrade, String studentName) {
        this(examGrade.getId(), examGrade.getExam().getId(), examGrade.getExam().getSubject().getName(), studentName, examGrade.getStudent().getId(),
                examGrade.getGrade(), examGrade.getPoints());
    }
    public TeacherExamGradesDTO() {
    }

    public Long getExamGradeId() {
        return examGradeId;
    }

    public void setExamGradeId(Long examGradeId) {
        this.examGradeId = examGradeId;
    }

    public Long getExamId() {
        return examId;
    }

    public void setExamId(Long examId) {
        this.examId = examId;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public Float getPoints() {
        return points;
    }

    public void setPoints(Float points) {
        this.points = points;
    }
}
