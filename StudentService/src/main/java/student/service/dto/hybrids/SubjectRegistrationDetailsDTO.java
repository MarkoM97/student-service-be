package student.service.dto.hybrids;

import student.service.model.Exam;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SubjectRegistrationDetailsDTO {
    private Long examId;
    private String name;
    private Long subjectCode;
    private String teacherName;
    private Date date;
    private String time;
    private String classRoom;
    private String register;

    public SubjectRegistrationDetailsDTO() {
    }

    public SubjectRegistrationDetailsDTO(Long examId,String name, Long subjectCode, String teacherName,
                                         Date date, String time, String classRoom, String register) {
        this.examId = examId;
        this.name = name;
        this.subjectCode = subjectCode;
        this.teacherName = teacherName;
        this.date = date;
        this.time = time;
        this.classRoom = classRoom;
        this.register = register;
    }

    public SubjectRegistrationDetailsDTO(Exam exam, String teacherName, String register) {
        this(exam.getId(),exam.getSubject().getName(), exam.getSubject().getSubjectCode(),
                teacherName, exam.getDate(), exam.getTime(), exam.getClassRoom(), register);
    }

    public static List<SubjectRegistrationDetailsDTO> toDTOs (List<Exam> exams, List<String> teacherNames, List<Long> registeredExamsId){
        List<SubjectRegistrationDetailsDTO> subjectDetailsDTOS = new ArrayList<>();
        for (int i = 0; exams.size() > i; i++){
            Date today = new Date();
            String register = "";
            if(registeredExamsId.contains(exams.get(i).getId())){
                register = "AlreadyRegistered";
            }
            else if(exams.get(i).getDate().before(today)){
                register = "NotAvailable";
            }
            else if(exams.get(i).getDate().after(today)){
                register = "Available";
            }
            SubjectRegistrationDetailsDTO subjectDetailsDTO = new SubjectRegistrationDetailsDTO(exams.get(i),teacherNames.get(i), register);
            subjectDetailsDTOS.add(subjectDetailsDTO);
        }
        return subjectDetailsDTOS;
    }

    public Long getExamId() {
        return examId;
    }

    public void setExamId(Long examId) {
        this.examId = examId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getSubjectCode() {
        return subjectCode;
    }

    public void setSubjectCode(Long subjectCode) {
        this.subjectCode = subjectCode;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getClassRoom() {
        return classRoom;
    }

    public void setClassRoom(String classRoom) {
        this.classRoom = classRoom;
    }

    public String getRegister() {
        return register;
    }

    public void setRegister(String register) {
        this.register = register;
    }
}
