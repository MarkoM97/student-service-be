package student.service.dto.hybrids;

import student.service.model.Exam;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;

public class TeacherExamDTO {
    private Long id;
    private String date;
    private String time;
    private String classRoom;
    private String subjectName;
    private Boolean allowInsertGrades;


    public TeacherExamDTO(Long id, Date date, String time, String classRoom, String subjectName) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        this.id = id;
        this.date = formatter.format(date);
        this.time = time;
        this.classRoom = classRoom;
        this.subjectName = subjectName;
        if(date.after(new Date())){
            this.allowInsertGrades = false;
        }
        else{
            this.allowInsertGrades = true;
        }
    }

    public TeacherExamDTO() {
    }

    public TeacherExamDTO(Exam exam){
        this(exam.getId(), exam.getDate(), exam.getTime(), exam.getClassRoom(), exam.getSubject().getName());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getClassRoom() {
        return classRoom;
    }

    public void setClassRoom(String classRoom) {
        this.classRoom = classRoom;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public Boolean getAllowInsertGrades() {
        return allowInsertGrades;
    }

    public void setAllowInsertGrades(Boolean allowInsertGrades) {
        this.allowInsertGrades = allowInsertGrades;
    }
}
