package student.service.dto.hybrids;

import student.service.dto.StudentDTO;
import student.service.dto.TeacherDTO;
import student.service.dto.UserDTO;

public class UserHybridDTO {
    private UserDTO user;
    private StudentDTO student;
    private TeacherDTO teacher;


    public UserHybridDTO() {
    }

    public UserHybridDTO(UserDTO user, StudentDTO student, TeacherDTO teacher) {
        this.user = user;
        this.student = student;
        this.teacher = teacher;
    }

    public UserDTO getUser() {
        return user;
    }

    public StudentDTO getStudent() {
        return student;
    }

    public TeacherDTO getTeacher() {
        return teacher;
    }


    public void setUser(UserDTO user) {
        this.user = user;
    }

    public void setStudent(StudentDTO student) {
        this.student = student;
    }

    public void setTeacher(TeacherDTO teacher) {
        this.teacher = teacher;
    }

    @Override
    public String toString() {
        return "UserHybridDTO{" +
                "user=" + user +
                ", student=" + student +
                ", teacher=" + teacher +
                '}';
    }
}
