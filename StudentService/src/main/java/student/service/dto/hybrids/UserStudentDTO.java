package student.service.dto.hybrids;

import java.util.Date;
import java.util.List;

public class UserStudentDTO {

    private String username;
    private String firstname;
    private String lastname;
    private Date birthday;
    private String email;
    private String password;
    private String address;
    private Boolean deleted;
    private String cardNumber;
    private String accountNumber;
    private String modelNumber;
    private Float balance;
    private Long studyProgram;

    public UserStudentDTO() {
    }

    public UserStudentDTO(String username, String firstname, String lastname, Date birthday,
                          String email, String password, String address, Boolean deleted,
                          String cardNumber, String accountNumber, String modelNumber, Float balance, Long studyProgram) {
        this.username = username;
        this.firstname = firstname;
        this.lastname = lastname;
        this.birthday = birthday;
        this.email = email;
        this.password = password;
        this.address = address;
        this.deleted = deleted;
        this.cardNumber = cardNumber;
        this.accountNumber = accountNumber;
        this.modelNumber = modelNumber;
        this.balance = balance;
        this.studyProgram = studyProgram;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getModelNumber() {
        return modelNumber;
    }

    public void setModelNumber(String modelNumber) {
        this.modelNumber = modelNumber;
    }

    public Float getBalance() {
        return balance;
    }

    public void setBalance(Float balance) {
        this.balance = balance;
    }

    public Long getStudyProgram() {
        return studyProgram;
    }

    public void setStudyProgram(Long studyProgram) {
        this.studyProgram = studyProgram;
    }
}
