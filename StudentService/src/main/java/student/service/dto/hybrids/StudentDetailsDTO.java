package student.service.dto.hybrids;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import student.service.dto.CourseAttendingDTO;
import student.service.dto.PaymentDTO;
import student.service.model.CourseAttending;
import student.service.model.User;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class StudentDetailsDTO {
    private Long id;
    private String firstname;
    private String lastname;
    private String birthday;
    private String email;
    private String address;
    private String cardNumber;
    private String accountNumber;
    private String modelNumber;
    private Float balance;
    private CourseAttendingDTO courseAttendingDTO;
    private List<PaymentDTO> payments;

    public StudentDetailsDTO() {
    }

    public StudentDetailsDTO(Long id, String firstname, String lastname, Date birthday, String email,
                             String address, String cardNumber, String accountNumber, String modelNumber,
                             Float balance, CourseAttendingDTO courseAttendingDTO, List<PaymentDTO> payments) {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.birthday = formatter.format(birthday);
        this.email = email;
        this.address = address;
        this.cardNumber = cardNumber;
        this.accountNumber = accountNumber;
        this.modelNumber = modelNumber;
        this.balance = balance;
        this.courseAttendingDTO = courseAttendingDTO;
        this.payments = payments;
    }

    public StudentDetailsDTO(User user, CourseAttendingDTO courseAttendingDTO, List<PaymentDTO> payments) {
        this(user.getId(), user.getFirstname(), user.getLastname(), user.getBirthday(),
                user.getEmail(), user.getAddress(), user.getStudent().getCardNumber(),
                user.getStudent().getAccountNumber(), user.getStudent().getModelNumber(),
                user.getStudent().getBalance(), courseAttendingDTO, payments);
    }

    public static Page<StudentDetailsDTO> toDTOS(Page<User> users, List<CourseAttendingDTO> courseAttendings){
        Page<StudentDetailsDTO> dtos = users.map(x -> new StudentDetailsDTO(x, null, null));

        int index = 0;
        for (Iterator<StudentDetailsDTO> iterator = dtos.iterator(); iterator.hasNext();) {
            StudentDetailsDTO studentDetailsDTO = iterator.next();
            studentDetailsDTO.setCourseAttendingDTO(courseAttendings.get(index));
            index ++;
        }
        return dtos;
    }

    public List<PaymentDTO> getPayments() {
        return payments;
    }

    public void setPayments(List<PaymentDTO> payments) {
        this.payments = payments;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getModelNumber() {
        return modelNumber;
    }

    public void setModelNumber(String modelNumber) {
        this.modelNumber = modelNumber;
    }

    public Float getBalance() {
        return balance;
    }

    public void setBalance(Float balance) {
        this.balance = balance;
    }

    public CourseAttendingDTO getCourseAttendingDTO() {
        return courseAttendingDTO;
    }

    public void setCourseAttendingDTO(CourseAttendingDTO courseAttendingDTO) {
        this.courseAttendingDTO = courseAttendingDTO;
    }
}
