package student.service.dto.hybrids;

import student.service.dto.TeacherTypeDTO;
import student.service.dto.UserDTO;
import student.service.model.User;

import java.util.Date;

public class UserTeacherDTO extends UserDTO {

    private Long teacherCode;
    private TeacherTypeDTO teacherTypeDTO;

    public UserTeacherDTO(Long id, String username, String firstname, String lastname, Date birthday, String email, String password, String address, Boolean deleted, Long teacherCode, TeacherTypeDTO teacherTypeDTO) {
        super(id, username, firstname, lastname, birthday, email, password, address, deleted);
        this.teacherCode = teacherCode;
        this.teacherTypeDTO = teacherTypeDTO;
    }

    public Long getTeacherCode() {
        return teacherCode;
    }

    public void setTeacherCode(Long teacherCode) {
        this.teacherCode = teacherCode;
    }

    public TeacherTypeDTO getTeacherTypeDTO() {
        return teacherTypeDTO;
    }

    public void setTeacherTypeDTO(TeacherTypeDTO teacherTypeDTO) {
        this.teacherTypeDTO = teacherTypeDTO;
    }
}
