package student.service.dto.hybrids;

import org.springframework.data.domain.Page;
import student.service.dto.CourseAttendingDTO;
import student.service.dto.SubjectDTO;
import student.service.model.User;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class TeacherDetailsDTO {
    private Long id;
    private String firstname;
    private String lastname;
    private String birthday;
    private String email;
    private String address;
    private String teacherType;
    private Long teacherCode;
    private List<SubjectDTO> subjectsDTO;

    public TeacherDetailsDTO() {
    }

    public TeacherDetailsDTO(Long id, String firstname, String lastname,
                             Date birthday, String email, String address, String teacherType,
                             Long teacherCode, List<SubjectDTO> subjectsDTO) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.birthday = formatter.format(birthday);
        this.email = email;
        this.address = address;
        this.teacherType = teacherType;
        this.teacherCode = teacherCode;
        this.subjectsDTO = subjectsDTO;
    }

    public TeacherDetailsDTO(User user, List<SubjectDTO> subjectsDTO) {
        this(user.getId(), user.getFirstname(),
                user.getLastname(), user.getBirthday(), user.getEmail(),
                user.getAddress(), user.getTeacher().getTeacherType().getName(),
                user.getTeacher().getTeacherCode(),  subjectsDTO);
    }

    public static Page<TeacherDetailsDTO> toDTOS(Page<User> users){
        Page<TeacherDetailsDTO> dtos = users.map(x -> new TeacherDetailsDTO(x, null));
        return dtos;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTeacherType() {
        return teacherType;
    }

    public void setTeacherType(String teacherType) {
        this.teacherType = teacherType;
    }

    public Long getTeacherCode() {
        return teacherCode;
    }

    public void setTeacherCode(Long teacherCode) {
        this.teacherCode = teacherCode;
    }

    public List<SubjectDTO> getSubjectsDTO() {
        return subjectsDTO;
    }

    public void setSubjectsDTO(List<SubjectDTO> subjectsDTO) {
        this.subjectsDTO = subjectsDTO;
    }
}
