package student.service.dto;

import student.service.model.TeacherType;

public class TeacherTypeDTO {

    private Long id;
    private String name;
    private Boolean deleted;

    public TeacherTypeDTO() {
    }

    public TeacherTypeDTO(Long id, String name, Boolean deleted) {
        this.id = id;
        this.name = name;
        this.deleted = deleted;
    }

    public TeacherTypeDTO(TeacherType teacherType) {
    	this(teacherType.getId(), teacherType.getName(), teacherType.getDeleted());
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
