package student.service.dto;

import student.service.mappers.CourseTypeMapper;
import student.service.model.CourseType;
import student.service.model.StudyProgram;

public class StudyProgramDTO {

    private Long id;
    private String name;
    private CourseTypeDTO courseType;
    private int duration;
    private Boolean deleted;

    public StudyProgramDTO() {
    }

    public StudyProgramDTO(Long id, String name, int duration, Boolean deleted, CourseType courseType) {
        this.id = id;
        this.name = name;
        this.duration = duration;
        this.deleted = deleted;
        this.courseType = CourseTypeMapper.toDto(courseType);
    }

    public StudyProgramDTO(StudyProgram studyProgram) {
        this(studyProgram.getId(), studyProgram.getName(), studyProgram.getDuration(), studyProgram.getDeleted(), studyProgram.getCourseType());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public CourseTypeDTO getCourseType() { return courseType; }

    public void setCourseType(CourseTypeDTO courseType) { this.courseType = courseType; }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
