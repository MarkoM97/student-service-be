package student.service.dto;

public class CreateStudetDTO {
    private String cardNumber;
    private String accountNumber;
    private String modelNumber;
    private Float balance;
    private Long studyProgram;

    public CreateStudetDTO() {
    }

    public CreateStudetDTO(String cardNumber, String accountNumber, String modelNumber, Float balance, Long studyProgram) {
        this.cardNumber = cardNumber;
        this.accountNumber = accountNumber;
        this.modelNumber = modelNumber;
        this.balance = balance;
        this.studyProgram = studyProgram;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getModelNumber() {
        return modelNumber;
    }

    public void setModelNumber(String modelNumber) {
        this.modelNumber = modelNumber;
    }

    public Float getBalance() {
        return balance;
    }

    public void setBalance(Float balance) {
        this.balance = balance;
    }

    public Long getStudyProgram() {
        return studyProgram;
    }

    public void setStudyProgram(Long studyProgram) {
        this.studyProgram = studyProgram;
    }
}
