package student.service.dto;

import student.service.model.Document;

public class DocumentDTO {
    private Long id;
    private String name;
    private Boolean deleted;

    public DocumentDTO() {
    }

    public DocumentDTO(Long id, String name, Boolean deleted) {
        this.id = id;
        this.name = name;
        this.deleted = deleted;
    }
    
    public DocumentDTO(Document document) {
    	this(document.getId(), document.getName(), document.getDeleted());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

}
