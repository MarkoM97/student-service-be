package student.service.dto;

import java.util.Date;

public class CreateExamDTO {
    private Long id;
    private Date date;
    private String time;
    private String classRome;
    private Boolean deleted;
    private Long subject;

    public CreateExamDTO(){};

    public CreateExamDTO(Long id, Date date, String time, String classRome, Boolean deleted, Long subject) {
        this.id = id;
        this.date = date;
        this.time = time;
        this.classRome = classRome;
        this.deleted = deleted;
        this.subject = subject;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getClassRome() {
        return classRome;
    }

    public void setClassRome(String classRome) {
        this.classRome = classRome;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Long getSubject() {
        return subject;
    }

    public void setSubject(Long subject) {
        this.subject = subject;
    }
}
