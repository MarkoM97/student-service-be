package student.service.dto;

import student.service.model.ExamGrade;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ExamPassedDTO {

    private String SubjectName;
    private Date examDate;
    private String teacherName;
    private Float points;
    private Integer grade;

    public ExamPassedDTO() {
    }

    public ExamPassedDTO(String subjectName, Date examDate, String teacherName, Float points, Integer grade) {
        SubjectName = subjectName;
        this.examDate = examDate;
        this.teacherName = teacherName;
        this.points = points;
        this.grade = grade;
    }

    public ExamPassedDTO(ExamGrade examGrade, String teacherName){
        this(examGrade.getExam().getSubject().getName(), examGrade.getExam().getDate(),
                teacherName, examGrade.getPoints(), examGrade.getGrade());
    }



    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

    public Date getExamDate() {
        return examDate;
    }

    public void setExamDate(Date examDate) {
        this.examDate = examDate;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public Float getPoints() {
        return points;
    }

    public void setPoints(Float points) {
        this.points = points;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }
}
