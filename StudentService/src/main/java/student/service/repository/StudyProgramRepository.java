package student.service.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import student.service.model.StudyProgram;

import java.util.List;

public interface StudyProgramRepository extends JpaRepository<StudyProgram, Long> {

    StudyProgram findByIdAndDeletedFalse(Long id);
    StudyProgram findByNameAndDeletedFalse(String name);
    Page<StudyProgram> findAllByDeletedFalse(Pageable pageable);
    List<StudyProgram> findByCourseType_IdAndDeletedFalse(Long id);
}
