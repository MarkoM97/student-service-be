package student.service.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import student.service.model.TeacherType;

import java.util.List;

public interface TeacherTypeRepository extends JpaRepository<TeacherType, Long> {

    TeacherType findByIdAndDeletedFalse(Long id);
    TeacherType findByNameAndDeletedFalse(String name);
    List<TeacherType> findAllByDeletedFalse();
}
