package student.service.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import student.service.model.ExaminationPeriod;

import java.util.Date;
import java.util.List;

public interface ExaminationPeriodRepository extends JpaRepository<ExaminationPeriod, Long> {
    Page<ExaminationPeriod> findAllByDeletedFalse(Pageable pageable);
    ExaminationPeriod findByIdAndDeletedFalse(Long id);
    List<ExaminationPeriod> findByEndDateAfterAndStartDateBetweenAndDeletedFalse(Date endDate, Date startDateBefore, Date startDateAfter);
}
