package student.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import student.service.model.Authority;

public interface AuthorityRepository extends JpaRepository<Authority, Long> {

    Authority findByName(String name);
}
