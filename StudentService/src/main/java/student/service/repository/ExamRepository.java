package student.service.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import student.service.model.Exam;

import java.util.List;

public interface ExamRepository extends JpaRepository<Exam, Long> {

    List<Exam> findByExaminationPeriod_IdAndDeletedFalse(Long id);
    List<Exam> findBySubject_IdAndDeletedFalse(Long id);
    Exam findByIdAndDeletedFalse(Long id);
    Page<Exam> findAllByDeletedFalse(Pageable pageable);
    List<Exam> findAllByDeletedFalse();
    List<Exam> findAllByDeletedFalseAndTeacherTeacherCode(Long teacherCode);
}
