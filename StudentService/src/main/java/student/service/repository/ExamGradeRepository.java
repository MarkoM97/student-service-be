package student.service.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import student.service.model.ExamGrade;

import java.util.List;

public interface ExamGradeRepository extends JpaRepository<ExamGrade, Long> {
    ExamGrade findByIdAndDeletedFalse(Long id);
    Page<ExamGrade> findAllByDeletedFalse(Pageable pageable);
    List<ExamGrade> findByExam_IdAndDeletedFalse(Long id);
    List<ExamGrade> findByStudent_IdAndDeletedFalse(Long id);
    List<ExamGrade> findByStudent_IdAndDeletedFalseAndGrade(Long id, Integer grade);
    List<ExamGrade> findByStudent_IdAndDeletedFalseAndGradeGreaterThanEqual(Long id, int grade);
}
