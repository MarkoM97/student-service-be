package student.service.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import student.service.model.Subject;

import java.util.List;

public interface SubjectRepository extends JpaRepository<Subject, Long> {
    Page<Subject> findAllByDeletedFalse(Pageable pageable);
    Subject findByIdAndDeletedFalse(Long id);
    List<Subject> findByTeacher_TeacherCodeAndDeletedFalse(Long teacherCode);
    List<Subject> findByStudyProgram_IdAndDeletedFalse(Long studentId);
}
