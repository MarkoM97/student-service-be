package student.service.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import student.service.model.Payment;

public interface PaymentRepository extends JpaRepository<Payment, Long> {

    Page<Payment> findAllByDeletedFalse(Pageable pageable);
    Payment findByIdAndDeletedFalse(Long id);
    Page<Payment> findAllByStudentIdAndDeletedFalse(Long studentId, Pageable pageable);
}
