package student.service.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import student.service.model.Teacher;
import student.service.model.TeacherType;

public interface TeacherRepository extends JpaRepository<Teacher, Long> {

    Page<Teacher> findAllByDeletedFalse(Pageable pageable);
    Teacher findByTeacherCodeAndDeletedFalse(long teacherCode);
}
