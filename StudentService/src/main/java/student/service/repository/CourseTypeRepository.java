package student.service.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import student.service.model.CourseType;

public interface CourseTypeRepository extends JpaRepository<CourseType, Long> {

    CourseType findByIdAndDeletedFalse (Long id);
    Page<CourseType> findAllByDeletedFalse(Pageable pageable);
}
