package student.service.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import student.service.model.User;
import student.service.model.UserAuthority;

public interface UserAuthorityRepository extends JpaRepository<UserAuthority, Long> {

    Page<UserAuthority> findByAuthority_NameAndUser_Deleted(String authorityName, boolean deleted, Pageable pageable);
}
