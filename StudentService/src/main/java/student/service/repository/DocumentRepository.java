package student.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import student.service.model.Document;
import student.service.model.ExamGrade;

import java.util.List;

public interface DocumentRepository extends JpaRepository<Document, Long> {
    List<Document> findByStudent_IdAndDeletedFalse(Long id);
}
