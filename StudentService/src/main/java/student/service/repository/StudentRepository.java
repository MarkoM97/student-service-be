package student.service.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import student.service.model.Student;

public interface StudentRepository extends JpaRepository<Student, Long> {

    Student findByIdAndDeletedFalse(Long id);
    Page<Student> findAllByDeletedFalse(Pageable pageable);
    Student findByCardNumberAndDeletedFalse(String cardNumber);
    void deleteByCardNumber(String cardNumber);
    Student findFirstByDeletedFalseOrderByIdDesc();
}
