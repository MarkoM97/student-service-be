package student.service.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import student.service.model.User;

import javax.validation.constraints.Null;
import java.util.List;

public interface UserRepository extends JpaRepository<User, Long> {

	Page<User> findAllByDeletedFalse(Pageable pageable);
	Page<User> findByDeletedFalseAndStudent_IdAndTeacher_TeacherCode(Pageable pageable, Long studentId, Long teacherId);
	User findByIdAndDeletedFalse(Long userId);
	User findByStudent_IdAndDeletedFalse(Long studentId);
	User findByTeacher_TeacherCodeAndDeletedFalse(Long teacherCode);
	User findByUsernameAndDeletedFalse(String username);
	List<User> findByTeacherAndStudentAndDeletedFalse(Long teacherId, Long studentId);
}
