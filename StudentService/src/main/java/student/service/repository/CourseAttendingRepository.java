package student.service.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import student.service.model.CourseAttending;

import java.util.List;

public interface CourseAttendingRepository extends JpaRepository<CourseAttending, Long> {
    CourseAttending findByIdAndDeletedFalse(Long id);
    CourseAttending findByStudent_IdAndDeletedFalse(Long studentId);
    Page<CourseAttending> findAllByDeletedFalse(Pageable page);

}
